module.exports = {
  purge: ['./src/components/**/*.{js,ts,jsx,tsx}', './pages/**/*.{js,ts,jsx,tsx}'],
  theme: {
    typography: {
      '2xl': {
        css: {
          img: {
            marginTop: 0,
            marginBottom: 0
          },
          a: {
            color: '#1c64f2',
          }
          // ...
        },
      },
      'md': {
        css: {
          img: {
            marginTop: 0,
            marginBottom: 0
          },
          a: {
            color: '#1c64f2',
          }
          // ...
        },
      },
    },
    extend: {
      screens: {
        sm: '640px',
        md: '768px',
        lg: '1024px',
        xl: '1280px',
        'bp': { 'min': '321px' },
      },
      maxWidth: {
        'xs': '17rem',
        '6xl': '75rem',
        '7xl': '24rem',
        '65ch': '65ch'
      },
      fontSize: {
        xs: '0.75rem',
        sm: '0.875rem',
        base: '1rem',
        lg: '1.125rem',
        xl: '1.25rem',
        '2xl': '1.5rem',
        '3xl': '1.875rem',
        '4xl': '2.25rem',
        '5xl': '3rem',
        '6xl': '4rem',
        'medium': '1.625rem'
      },
      height: {
        '6xl': '400px',
        '7xl': '500px',
      }
    }
  },
  variants: {
    extend: {
      fontWeight: ['hover', 'focus'],
      borderWidth: ['responsive', 'first', 'last', 'hover', 'focus'],
    padding: ['responsive', 'first', 'last', 'hover', 'focus'],
    textColor: ['responsive', 'hover', 'focus', 'group-hover'],
     },
    
  },
  plugins: [
    require('@tailwindcss/ui'),
    require('@tailwindcss/typography'),
    require('@tailwindcss/forms'),
  ]
}
