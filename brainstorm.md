# Flow


1. New User
2. Registrered user

# Landing page
1. Who we are.
2. what are we offereing
3. FOur Buttons
   1. I am professional
   2. I am company
   3. Veiw all professiona profiles
   4. View all Companies list
4. Two section
   1. top 10 profile
   2. top companies
5. Link to 3.3 and 3.4

## View all profiles
1. Basic filters
   1. COmpany
   2. Professional/Student
   3. Name
   4. Submit to have result
2. Result
   1. Picture
   2. Name
   3. COmpany
   4. Designation
   5. View details button to pop up new page
   6. All details of profile except contact details
   7. To view contact register yourself
3. Every profile footer will have CTA ( Create your own Profile)
## View all companies
1. Basic filters
   1. COmpany
   2. Submit to have result
2. Result
   1. Picture
   2. COmpany
   4. View details button to pop up new page
   6. All details of company except contact details
   7. To view contact click on button
3. Every company footer will have CTA ( register your own Company)

# Register

## Register your profile.

1. Step one Enter your email address/
2. Step two open your email box to continue with registration process
3. Redirect to dashboard
   1. Manage Profile
      1. View your own profile
      2. Integration with Social media.
   2. Apply for Job ( phase 2)
   
## Register your Company.

1. Step one Enter your email address/
2. Step two open your email box to continue with registration process
3. Redirect to dashboard
   1. Manage Company
      1. View your own Company
      2. Integration with Social media.
   2. Post a Job ( phase 2)


