const useForm = (dispatch) => {
  const onChange = (e) => {
    dispatch({
      type: 'setFormValue',
      payload: {
        [e.target.name]: e.target.value 
      } 
    })
  }

  return { onChange };
}

export { useForm };