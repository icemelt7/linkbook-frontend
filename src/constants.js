export const basePicUrl = "https://res.cloudinary.com/dwqo3od7a/image/upload/";

export const listingThumbForProfilePic = (path, size) => {

  if(path.includes('licdn')){
    return path
  }

  if (size === "small") {
    return `${basePicUrl}c_thumb,g_face,h_60,w_64/${path}`;
  }
  return `${basePicUrl}c_thumb,g_face,h_120,w_120/${path}`;
}

export const fixListingImageUrl = (path) => {
  return path.split("/").slice(0, 6).concat(['c_thumb,g_face,h_60,w_64']).concat(path.split("/").slice(7)).join('/')
}

export const userProfileFields = [
  'email',
  'name',
  'employed',
  'experience',
  'phone_number',
  'location',
  'summary',
  'current_job_company_beta',
  'current_job_company', // Right now this is just a string. Not sure how this will work. I'll create a new request to create a new company, for user-update I'll see what is sanity expecting
  'current_job_desc',
  'current_job_department',
  'industry',
  'sector',
  'interested_in',
  'headline',
  'picture'
]