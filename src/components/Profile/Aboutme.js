import { isAvail } from "../../lib/helpers";
import WhiteCard from "./WhiteCard";

const Aboutme = ({ user }) => {
  return <div className="md:w-3/5 px-2 mb-4">
    <WhiteCard>
      <p className="text-3xl font-medium mb-2">About Me</p>
      <p
        className="text-base leading-relaxed"
        style={{
          maxWidth: "57ch"
        }}
      >
        {isAvail(user.summary)}
      </p>
    </WhiteCard>
  </div>
}

export default Aboutme;