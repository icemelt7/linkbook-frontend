import { isUpperCase, isAvail } from "../../lib/helpers";
import WhiteCard from "./WhiteCard";

const BasicInformationLoggedOut = ({ user }) => {
  return (
    <WhiteCard>
      <p className="text-3xl font-medium mb-2">Basic Information</p>
      <p className="text-xl font-medium">Information: </p>
      <p className="text-base mb-2">{`${isUpperCase(
        user.current_job_desc
      )} at ${isUpperCase(user.current_job_company)}`}</p>
      <p className="text-xl font-medium">Headline:</p>
      <p className="text-base mb-4">{isUpperCase(user.headline)}</p>
      <div  className="bg-blue-600 hover:bg-blue-700 text-white font-medium py-2 px-16 rounded shadow text-center">
        <a
          href="/emailsignin"
         
        >
          Login to reveal contact info
          </a>
          </div>
    </WhiteCard>
  );
};

export default BasicInformationLoggedOut;