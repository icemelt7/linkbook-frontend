import { isUpperCase, isAvail } from "../../lib/helpers";
import WhiteCard from "./WhiteCard";



const BasicInformation = ({ user }) => {

  return (
    <WhiteCard>
      <p className="text-3xl font-medium mb-2">Basic Information</p>
      <p className="text-xl font-medium">Information: </p>
      <p className="text-base mb-2">{`${isUpperCase(
        user.current_job_desc
      )} at ${isUpperCase(user.current_job_company_beta.name)}`}</p>
      <p className="text-xl font-medium">Headline:</p>
      <p className="text-base mb-2">{isUpperCase(user.headline)}</p>
      <p className="text-xl font-medium">Email: </p>
      <p className="text-base mb-2">{isAvail(user.email)}</p>
      <p className="text-xl font-medium">Phone: </p>
      <p className="text-base mb-2">{isAvail(user.phone_number)}</p>
      <p className="text-xl font-medium">Location: </p>
      <p className="text-base mb-2">{isAvail(user.location)}</p>
    </WhiteCard>
  );
};

export default BasicInformation;
