const WhiteCard = ({ children }) => {
  return <div className="bg-white p-8 rounded-lg shadow">
    {children}
  </div>
}

export default WhiteCard;