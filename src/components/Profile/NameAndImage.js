import { isUpperCase } from "../../lib/helpers";
import { fixListingImageUrl, listingThumbForProfilePic } from "../../constants";

const NameAndImage = ({ user }) => {
  let imgUrl = null;
  if (user.picture) {
    const { picture } = user;
    if (picture.includes("dwqo3od7a")) {
      imgUrl = fixListingImageUrl(picture);
    } else if (picture.includes("licdn")) {
      imgUrl = null;
    } else {
      imgUrl = listingThumbForProfilePic(picture);
    }
  }
  return <div className="mx-auto bg-white overflow-hidden rounded-lg shadow">
  <div className="sm:flex sm:items-center mx-16 py-4">
    <div className="relative">
      {imgUrl && (
        <img
          className="block mx-auto mb-4 sm:mx-0 sm:flex-shrink-0 h-16 sm:h-24 rounded-full shadow-inner"
          src={imgUrl}
        />
      )}
      <img
        className="sm:h-8 h-6 sm:absolute mx-auto bottom-0 right-0"
        style={{
          filter: "drop-shadow(0 0 4px #c3c3c3)",
          cursor: "pointer"
        }}
        src="/static/images/icons8-share-64.png"
      />
    </div>
    <div className="mt-4 sm:mt-0 sm:ml-4 text-center sm:text-left">
      <p className="text-xl leading-tight">{isUpperCase(user.name)}</p>
      <p className="text-sm leading-tight text-gray-600">
        {isUpperCase(user.headline)}
      </p>
    </div>
  </div>
</div>
}

export default NameAndImage;