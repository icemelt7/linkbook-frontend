import Link from "next/link";

const BackButton = () => (
  <div className="flex justify-center mx-16 mb-8">
    <Link
      href="/profilelist"
    >
      <a className="bg-red-500 hover:bg-red-700 text-white font-medium py-2 px-16 rounded">Go back</a>
    </Link>
  </div>
);

export default BackButton;
