import Profile from "../ProfileList/Profile";
import Link from "next/link";

const FeaturedProfiles = ({ profiles }) => {
  return <div>
    <p className="w-full text-5xl font-bold leading-tight text-center text-gray-800 mb-8">Featured Profiles</p>
    {profiles.map(profile => {
      return <Profile key={profile.objectID} profile={profile} />
    })}
    <div className="text-center mt-4">
      <Link href="/profilelist">
        <a className="mx-auto lg:mx-0 hover:underline bg-white text-gray-800 font-bold rounded-full my-6 py-4 px-8 shadow-lg">
          Check out all the profiles
            </a>
      </Link>
    </div>
  </div>
}

export default FeaturedProfiles;