function NewFooter() {
    return (
        <div className="mx-auto bg-gray-900 overflow-hidden">
            <div className="flex justify-center px-6 sm:pb-4 pb-2 pt-4">
                <div className="mt-0 text-center">
                    <p className="text-3xl text-gray-500">More from Automark</p>
                </div>
            </div>
            <div className="px-16">
                <div className="flex flex-wrap text-center -mx-2">
                    <a href="#" className="hover:underline no-underline block text-gray-600 text-base sm:w-1/5 w-full sm:mt-0 mt-2 px-2">Magazine</a>
                    <a href="#" className="hover:underline no-underline block text-gray-600 text-base sm:w-1/5 w-full sm:mt-0 mt-2 px-2">Subscription Form</a>
                    <a href="#" className="hover:underline no-underline block text-gray-600 text-base sm:w-1/5 w-full sm:mt-0 mt-2 px-2">Subscription Form</a>
                    <a href="#" className="hover:underline no-underline block text-gray-600 text-base sm:w-1/5 w-full sm:mt-0 mt-2 px-2">Auto Industry News</a>
                    <a href="#" className="hover:underline no-underline block text-gray-600 text-base sm:w-1/5 w-full sm:mt-0 mt-2 px-2">Price List</a>
                </div>
            </div>
            <div className="border-gray-600 border-t h-0 my-4 mx-16"></div>
            <div className="flex justify-center px-6 mt-0 text-center mb-4">
                <p className="text-xl text-gray-500">Follow us on</p>
            </div>
            <div className="flex flex-wrap justify-center px-2 mb-4">
                <a href="#" className="hover:underline no-underline text-gray-600 text-base px-2"><img src="https://img.icons8.com/color/24/000000/facebook-circled.png" /></a>
                <a href="#" className="hover:underline no-underline text-gray-600 text-base px-2"><img src="https://img.icons8.com/color/24/000000/twitter.png" /></a>
                <a href="#" className="hover:underline no-underline text-gray-600 text-base px-2"><img src="https://img.icons8.com/color/24/000000/instagram-new.png" /></a>
            </div>
            <div className="flex justify-center text-center mb-4">
                <p className="text-sm text-gray-600">Built with love by team <a href="#" className="hover:underline no-underline text-gray-600">Automark</a></p>
            </div>
        </div>
    )
}
export default NewFooter;