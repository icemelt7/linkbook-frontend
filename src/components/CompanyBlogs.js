import React from 'react';
import format from 'date-fns/format';
import Link from "next/link";
import { Image } from './Blog/shared.js';
import { imgTransform } from '../lib/helpers.js';
import FireIcon from './Blog/FireIcon.js';

function CompanyBlogs({ slug, postImgUrl, postTitle, date }) {
  const mobileImgUrl = (w, h, mobile) => imgTransform(w, h, postImgUrl);
  return (
    <div key={slug} className="relative md:flex mb-12 bp:mb-13">
      <Label value="Update" />
      {postImgUrl && <Image 
        mobileImgUrl={mobileImgUrl} 
        postTitle={postTitle} 
      />}
      <div className="md:flex md:flex-wrap md:content-between md:mx-0  max-w-xs md:max-w-none md:w-1/2 mx-auto">
        <Title postTitle={postTitle} slug={slug} />
      </div>
    </div>
  )
}

const Title = ({ postTitle, slug }) => {
  return (
    <div className="md:pl-6 md:w-9/12 md:mx-0 md:pt-6">
      <Link key={slug} href="/[blogslug]" as={`/${slug}`}>
        <a className="antialiased text-lg font-bold leading-6 bp:leading-7">{postTitle}</a>
      </Link>
    </div>
  )
}

const Label = ({ value }) => {
  return (
    <div className="uppercase hidden absolute md:right-2 md:block font-sans bg-gray-900 text-white antialiased text-xss rounded top-2 px-3 h-6 leading-6">{value}</div>
  )
}

export default CompanyBlogs;