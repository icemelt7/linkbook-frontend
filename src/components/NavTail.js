import { string_to_slug, WP_CONTENT_API } from "../lib/helpers";

import { useFirebaseUser } from "../lib/hooks";
import useLinkbookReducer from "../lib/linkbookReducer";
import Link from "next/link";
import { useRouter } from "next/router";
import { useState } from "react";

const NavLink = ({ isLogin, name }) => {
  const router = useRouter();
  return (
    <ul>
      {isLogin && name !== "" && (
        <li
          className={
            router && router.pathname.includes("/profile/")
              ? "uppercase block sm:inline-block sm:mt-0 bg-blue-900 text-white p-4 mr-1 text-center"
              : "uppercase block bg-blue-700 sm:inline-block sm:mt-0 text-white hover:text-red-200 p-4 mr-1 text-center"
          }
        >
          <Link
            href={`/profile/[pid]`}
            as={`/profile/${string_to_slug(name || "", "-")}`}
            passHref
          >
            <a>View my Profile</a>
          </Link>
        </li>
      )}
      <li
        className={
          router && router.pathname == "/profilelist"
            ? "uppercase block sm:inline-block sm:mt-0 bg-red-700 text-white p-4 mr-1 text-center"
            : "uppercase block sm:inline-block sm:mt-0 text-white hover:text-red-200 p-4 mr-1 text-center"
        }
      >
        <Link href="/profilelist">
          <a>Profile List</a>
        </Link>
      </li>
      {isLogin && (
        <li
          className={
            router && router.pathname == "/dashboard"
              ? "uppercase block sm:inline-block sm:mt-0 bg-red-700 text-white p-4 mr-1 text-center"
              : "uppercase block sm:inline-block sm:mt-0 text-white hover:text-red-200 p-4 mr-1 text-center"
          }
        >
          <Link href="/dashboard">
            <a>Dashboard</a>
          </Link>
        </li>
      )}
    </ul>
  );
};

function NavTail() {
  const [open, setOpen] = useState(false);
  let show = "";

  function handleClick() {
    setOpen(!open);
  }

  show = open ? "block" : "hidden";
  const [user, dispatch] = useLinkbookReducer();
  const { current } = user;
  useFirebaseUser(dispatch);

  return (
    <nav
      className="flex items-center justify-between flex-wrap sticky top-0 shadow-lg z-10"
      style={{
        backgroundImage: "-webkit-linear-gradient(top, #e32a33 0%,#da191f 100%)"
      }}
    >
      <div className="flex items-center flex-shrink-0 text-white mr-6 p-4 ">
        <Link href="/">
          <a>
            <img
              className="w-32 mr-2"
              src={`${WP_CONTENT_API()}/uploads/2018/02/Automark_New_logo.jpg`}
              alt="automark logo"
            />
          </a>
        </Link>
      </div>
      <div className="block sm:hidden">
        <button
          className="fill-current flex items-center px-3 py-2 border rounded text-white border-white hover:text-white hover:border-white mr-4"
          onClick={handleClick}
        >
          <svg
            className="h-3 w-3"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Menu</title>
            <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
          </svg>
        </button>
      </div>

      <div
        className={`w-full ${show} flex-grow sm:flex sm:items-center sm:w-auto`}
      >
        {current === "initial" ? (
          <div className="text-sm sm:flex-grow">
            <p className="text-white text-xl">Loading..</p>
          </div>
        ) : current === "loaded" ? (
          <div className="text-sm sm:flex-grow sm:flex sm:justify-center">
            <NavLink isLogin={true} name={user.name} />
          </div>
        ) : (
              <div className="text-sm sm:flex-grow sm:flex sm:justify-center">
                <NavLink isLogin={false} />
              </div>
            )}
        {current === "initial" ? (
          <div>
            <p className="text-white text-xl sm:pr-4 sm:p-0 p-4">Loading..</p>
          </div>
        ) : current === "loaded" ? (
          <div>
            <p className="text-white block sm:inline-block uppercase sm:pr-4 sm:p-0 p-4">
              Logged in as: {user.name}
            </p>
          </div>
        ) : (
              <div className="text-center">
                <a
                  href="/emailsignin"
                  className="uppercase inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-red-500 hover:bg-white sm:mr-4 sm:m-0 m-4"
                >
                  Login
            </a>
              </div>
            )}
      </div>
    </nav>
  );
}

export default NavTail;
