import Link from "next/link";

function FooterBlog() {
  let showFooter = false;
  return (
    <div className="mx-auto bg-cool-gray-900 overflow-hidden mt-10">
      <div className="flex justify-center px-12 mb-16">
        <div className=" bg-orange-500 h-1 w-full"></div>
      </div>
      {showFooter && (
        <div>
          <p className="block bp:text-4xl bp:font-bold bp:leading-6 bp:mt-10 mb-4 px-12 text-white">
            Get Newsletter
          </p>
          <div className="border-b border-orange-500 w-8 h-1 mx-12 mb-8"></div>
          <p className="text-lg text-gray-300 font-sans font-normal px-12 mb-8">
            Get the latest news and updates
          </p>
          <div className="flex mx-12 mb-8">
            <input
              type="text"
              className="appearance-none bg-cool-gray-900 border-b-2 border-gray-600 pb-3 placeholder-white font-sans mr-4 text-xs w-full focus:outline-none text-white focus:border-gray-600"
              placeholder="Type your e-mail address here"
            />
            <button className="bg-gray-700 px-5 py-3 tracking-wider font-sans text-white text-xs">
              SUBSCRIBE
            </button>
          </div>
        </div>
      )}

      <div className="flex w-32 sm:w-full flex-wrap justify-center sm:justify-evenly mx-auto text-gray-600 font-sans text-left leading-9 pb-10 border-b mb-10 border-gray-700">
        <div className="mb-8 sm:mb-0">
          <p className="text-xl sm:text-base font-bold text-gray-200">
            <Link href="/contact-us" prefetch={false}>
              <a>Our Contact</a>
            </Link>
          </p>
          {/* <p><a href="#">General</a></p> */}
        </div>
        <div className="mb-8 sm:mb-0">
          <p className="text-xl sm:text-base font-bold text-gray-200">
            <Link href="/motorcycle" prefetch={false}>
              <a>List of New Motorcycles</a>
            </Link>
          </p>
        </div>
        <div className="mb-8 sm:mb-0">
          <p className="text-xl sm:text-base font-bold text-gray-200">
            <Link href="/car" prefetch={false}>
              <a>List of New Cars</a>
            </Link>
          </p>
        </div>
        <div className="mb-8 sm:mb-0">
          <p className="text-xl sm:text-base font-bold text-gray-200">
            <Link href="/company_type" prefetch={false}>
              <a>Company Profiles</a>
            </Link>
          </p>
        </div>
        <div className="mb-2 sm:mb-0">
          <p className="text-xl sm:text-base w-64 sm:w-full font-bold text-gray-200">
            <Link href="/category/[...params]" as="/category/magazine-2" prefetch={false}>
              <a>Automark Magazines</a>
            </Link>
          </p>
        </div>
      </div>

      <p className="w-full bp:text-2xl bp:font-bold bp:leading-6 bp:mt-10 mb-4 px-12 text-white text-center">
        Follow Us Today!
      </p>
      <div className="flex w-full justify-center">
        <div className="border-b border-orange-500 w-8 h-0 mb-10"></div>
      </div>

      <div className="flex flex-wrap justify-center px-2 mb-4">
        <a title="Automark Facebook"
          href="https://www.facebook.com/automarkmagazine/"
          className="hover:underline no-underline text-gray-600 text-base px-2"
        >
          <img loading="lazy" src="https://img.icons8.com/color/24/000000/facebook-circled.png" alt="Automark Facebook" />
        </a>
        <a title="Automark Twitter"
          href="https://twitter.com/automarkpk"
          className="hover:underline no-underline text-gray-600 text-base px-2"
        >
          <img loading="lazy" src="https://img.icons8.com/color/24/000000/twitter.png" alt="Automark Twitter"  />
        </a>
        <a title="Automark Instagram"
          href="https://www.instagram.com/automarkpk/"
          className="hover:underline no-underline text-gray-600 text-base px-2"
        >
          <img loading="lazy" src="https://img.icons8.com/color/24/000000/instagram-new.png" alt="Automark Instagram" />
        </a>
        <a title="Automark Youtube Channel"
          href="https://www.youtube.com/user/hanifmemon123"
          className="hover:underline no-underline text-gray-600 text-base px-2"
        >
          {/* <img loading="lazy" src={require("../icons/youtube.png")} alt="Automark Youtube Channel" /> */}
        </a>
      </div>
      <div className="flex justify-center text-center mb-4">
        <p className="text-sm text-gray-600">
          Built with love by team{" "}
          <Link href="/" prefetch={false}>
            <a className="hover:underline no-underline text-gray-600">
              Automark
            </a>
          </Link>
        </p>
      </div>
    </div>
  );
}
export default FooterBlog;
