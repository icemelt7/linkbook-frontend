import React from "react";

const FilterSearchButton = ({ toggleFilter }) => (
  <div className="w-full p-2 pb-4">
    <button
      onClick={toggleFilter}
      className="bg-white hover:bg-gray-100 text-gray-800 text-base py-2 px-4 w-full border border-gray-600 rounded shadow"
    >
      <span>
        <img
          src="https://img.icons8.com/material-rounded/24/000000/filter.png"
          className="inline-block pr-1 h-5 mb-1"
        />
      </span>
      FILTER SEARCH
    </button>
  </div>
);

export default FilterSearchButton;
