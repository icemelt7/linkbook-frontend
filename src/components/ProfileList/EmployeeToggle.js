import React, { useState, useEffect } from "react";

import { connectToggleRefinement } from "react-instantsearch-dom";

const EmployedToggle = connectToggleRefinement(
  ({ currentRefinement, refine }) => {
    const [values, setValues] = useState({
      professional: currentRefinement ? "professional" : "unemployed"
    });

    useEffect(() => {
      setValues({
        professional: currentRefinement ? "professional" : "unemployed"
      });
    }, [currentRefinement]);

    const handleChange = name => event => {
      setValues({ ...values, [name]: event.target.value });
      refine(!currentRefinement);
    };


    return (
      <div className="mb-8">
          <div className="py-2 text-lg pl-4">Category</div>
          <form className="px-4 text-gray-700">
            <div className="custom-control custom-radio py-3 sm:py-3 border-gray-300" value={values.professional} onClick={handleChange("professional")}>
              <input type="radio" className="custom-control-input cursor-pointer" id="customRadio" name="professional" value="professional" />
              <label className="custom-control-label pl-2 cursor-pointer" htmlFor="customRadio">Working in a company</label>
            </div>

            <div className="custom-control custom-radio py-3 sm:py-3 " value={values.professional} onClick={handleChange("professional")}>
              <input type="radio" defaultChecked className="custom-control-input cursor-pointer" id="defaultRadio" name="professional" value="unemployed" />
              <label className="custom-control-label pl-2 cursor-pointer" htmlFor="defaultRadio">Everyone</label>
            </div>
          </form>
      </div>
    );
  }
);

export default EmployedToggle;
