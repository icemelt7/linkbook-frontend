import React from "react";
import { connectNumericMenu } from "react-instantsearch-dom";

const NumericMenu = ({ items, refine }) => {
  
  return  <div className="mb-4">
    <div className="text-lg ml-4">Range</div>
    <form className="px-4 py-4 sm:py-2">
      {items.map((item,i) => (       
        <div key={i} className="custom-control custom-radio py-3 sm:py-3 text-gray-700" onClick={event => {
          event.preventDefault();
          refine(item.value);
        }}
        >
          <input
            type="radio"
            className="custom-control-input cursor-pointer"
            checked={item.isRefined}
            id={item.value}
            readOnly
          />
          <label
            className="custom-control-label pl-2 cursor-pointer"
            htmlFor={item.value}
            
          >
            {item.label}
          </label>
        </div>
      ))}
    </form>
  </div>
};

// 2. Connect the component using the connector
const CustomNumericMenu = connectNumericMenu(NumericMenu);

export default CustomNumericMenu;
