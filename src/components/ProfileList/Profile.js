import { string_to_slug } from '../../lib/helpers';
import Link from "next/link";
import { listingThumbForProfilePic, fixListingImageUrl } from "../../constants";

const Profile = ({ profile }) => {
  const isAvail = (str) => {
    if (str === "") {
      str = "Information not Available"
    }
    return str
  }
  const isUpperCase = (str) => {
    if (!str) {
      return '';
    }
    isAvail(str)
    if (str === str.toUpperCase()) {
      str = str.toLowerCase()
        .split(' ')
        .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
        .join(' ');
    } else if (str === str.toLowerCase()) {
      str = str
        .split(' ')
        .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
        .join(' ');
    }
    return str;
  }

  let imgUrl = null;
  const { picture } = profile;
  if (picture) {
    if (picture.includes('dwqo3od7a')) {
      imgUrl = fixListingImageUrl(picture);
    } else if (picture.includes('licdn')) {
      imgUrl = null;
    } else {
      imgUrl = listingThumbForProfilePic(picture, 'small');
    }
  }
  
  return (
    <div className="pb-6 px-6">
      <div className="bg-white rounded-lg shadow-lg p-4">
        <div className="sm:flex">
      {imgUrl &&
        <img 
          className="h-16 w-16 sm:h-24 sm:w-24 sm:mr-4 rounded-full sm:mx-0 mx-auto shadow-lg" 
          src={imgUrl} />}

      <div className="sm:text-left text-center">
        <h2 className="text-2xl">{isUpperCase(profile.name)}</h2>
        <div className="text-gray-700 text-base">{isUpperCase(profile.headline)}</div>
        {/* <div className="text-gray-600">{`${isUpperCase(profile.current_job_company)} at ${isUpperCase(profile.current_job_desc)}`}</div> */}
        <div className="text-gray-600 text-sm">{`${isAvail(profile.location)}`}</div>
      </div>
      </div>
      <div className="mt-4 flex sm:justify-start justify-center">
        <Link
          href={`/profile/[pid]`}
          as={`/profile/${string_to_slug(profile.name || '', '-')}`}
          passHref
        >
          <a className="bg-red-500 hover:bg-red-700 text-white font-medium py-2 px-4 rounded">View Profile</a>
        </Link>
      </div>
      </div>
    </div>

  );
};

export default Profile;
