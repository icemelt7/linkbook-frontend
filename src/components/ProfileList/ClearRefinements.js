import React from 'react';
import { connectCurrentRefinements } from 'react-instantsearch-dom';

const ClearRefinements = ({ items, forceClearRefine }) => {
  // return the DOM output
  return (
    <button className="text-red-600 text-base sm:py-2 sm:px-16"
      onClick={() => forceClearRefine(items)}
      disabled={!items.length}
    >
      Clear Search
    </button>
  );
};

const CustomClearRefinements = connectCurrentRefinements(ClearRefinements);

export default CustomClearRefinements;