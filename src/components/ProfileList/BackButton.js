import React from "react";

const BackButton = ({ toggleFilter }) => (
  <button onClick={toggleFilter} className="sm:hidden block">
    <img src="https://img.icons8.com/metro/26/000000/delete-sign.png" />
  </button>
);

export default BackButton;

