import React from "react";

const CollapsableBox = ({ children, open }) => {
  let show = open ? "100vh" : "0";
  let viewPort = open ? "0" : "100%";
  return (
    <div
      className="w-full h-screen bg-white absolute left-0 right-0 bottom-0 block z-10 overflow-hidden"
      style={{ top: viewPort, transition: "0.5s", height: show }}
    >
      {children}
    </div>
  );
};

export default CollapsableBox;
