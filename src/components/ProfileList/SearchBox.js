import React, { useState, useEffect } from 'react';
import { connectSearchBox } from "react-instantsearch-dom";

const CustomSearchBox = connectSearchBox(
  ({ INSTANT_MODE, currentRefinement, refine }) => {
    const [values, setValues] = useState({
      name: currentRefinement
    });

    useEffect(() => {
      setValues({ name: currentRefinement });
    }, [currentRefinement]);

    const handleChange = name => event => {
      setValues({ ...values, [name]: event.target.value });
      if (!INSTANT_MODE) {
        refine(event.target.value);
      }
    };

    const search = e => {
      e.preventDefault();
      refine(values.name);
    };

    return (
      <div className="w-full px-4">
        <div className="text-lg mb-4">Keyword Search</div>
        <form className="bg-white rounded" onSubmit={search}>
          <div>
            <input
              style={{
                backgroundImage:
                  "url(https://img.icons8.com/material/24/000000/search--v1.png)",
                backgroundPositionX: "5px",
                backgroundPositionY: "50%",
                backgroundRepeat: "no-repeat",
                paddingLeft: "32px"
              }}
              className="appearance-none border rounded w-full py-2 px-3 mb-4 text-gray-700 leading-tight focus:outline-none focus:border-gray-800"
              placeholder="Search"
              id="name"
              name="Name"
              value={values.name}
              onChange={handleChange("name")}
            />
            
            <button className="border w-full bg-blue-600 text-white text-base py-2 px-4 rounded">
              Search
            </button>
          </div>
        </form>
      </div>
    );
  }
);

export default CustomSearchBox;