import { connectStateResults } from 'react-instantsearch-dom';
import RiseLoader from "react-spinners/RiseLoader";

const StateResults = ({ searching, searchResults, searchState  }) => {
  const noResults = searchResults && searchResults.nbHits === 0;
  const hitcount = searchResults && searchResults.nbHits;
  return (
    <>
      {searching && noResults && <div className="flex justify-center mt-8"><RiseLoader color="red" /></div>}
  {searchState.query && <div className="text-center text-base py-2">Found <span className="font-bold">{hitcount}</span> Profiles</div>}      
    </>
  );
};

const CustomStateResults = connectStateResults(StateResults);
export default CustomStateResults;