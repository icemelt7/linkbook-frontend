import React from 'react';
import { connectHits } from "react-instantsearch-dom";
import Profile from './Profile';

const Hits = ({ hits }) => (
  <>{hits && hits.map(hit => <Profile key={hit.objectID} profile={hit} />)}</>
);
const CustomHits = connectHits(Hits);

export default CustomHits;