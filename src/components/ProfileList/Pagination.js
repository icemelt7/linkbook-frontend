import React from 'react';
import { connectPagination } from "react-instantsearch-dom";

let pageNumber = []
const Pagination = ({ currentRefinement, nbPages, refine, createURL }) => (
  <div className="text-center flex justify-center pb-4">
    {new Array(nbPages).fill(null).map((_, index) => {
      const page = index + 1;
      pageNumber.push(page)
      return (
        
          <a key={index}  href={createURL(page)}
            className={`${currentRefinement === page ? "font-bold" : "font-normal"} -ml-1 uppercase inline-block text-sm px-4 py-2 border ${index==0 ? "rounded" : ""} ${pageNumber[pageNumber.length-1]==nbPages ? "rounded-tr rounded-br" : "" } text-blue-600 border-gray-500 hover:text-blue-700 bg-white`}
            onClick={event => {
              event.preventDefault();
              refine(page);
            }}>{page}</a>
        
        // <Button
        //   variant="outlined"
        //   key={index}
        //   href={createURL(page)}
        //   style={style}
        //   onClick={event => {
        //     event.preventDefault();
        //     refine(page);
        //   }}
        // >
        //   {page}
        // </Button>



      );
    })}
  </div>
);

const CustomPagination = connectPagination(Pagination);

export default CustomPagination;