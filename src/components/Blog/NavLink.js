import React from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'

const NavLink = ({ href, as, children, activeClassName }) => {
  const router = useRouter()

  let className = children.props.className || ''
  if (as && router.asPath === as) {
    className = `${className} ${activeClassName}`
  }

  if (!as && router.pathname === href) {
    className = `${className} ${activeClassName}`
  } 

  return <Link href={href} as={as}>{React.cloneElement(children, { className })}</Link>
}

export default NavLink;