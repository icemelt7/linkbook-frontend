import Link from "next/link";
import PropTypes from "prop-types";

import styles from "./Img.module.css";

const Img = (props) => {
    const { href, as, src, alt, width, height, ...rest } = props;
    
    let linkedImage = <picture>
        <source media="(max-width: 799px)" srcSet={src(width, height, true)} />
        <source media="(min-width: 800px)" srcSet={src(width, height)} />
        <img 
            src={src(width, height)} 
            alt={alt}
            loading="lazy" 
            width={width}
            height={height}
            className={`${styles.img} ${rest.className}`}
        />
    </picture>;
    if (href) {
        linkedImage = <Link href={href} as={as}>
            <a>
                {linkedImage}
            </a>
        </Link>
    }
    return (
        <div className={`${styles.root} mx-auto mb-2 md:mx-0`} style={{ "--maxWidth": width }}>
            <div
                className={styles.wrapper}
                style={{
                    "--height": height,
                    "--width": width
                }}
            >
            {linkedImage}
            </div>
        </div>
    );
}

Img.propTypes = {
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    src: PropTypes.func.isRequired,
    alt: PropTypes.string.isRequired
};
export default Img;