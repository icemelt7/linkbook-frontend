import parse from 'html-react-parser';
import { WP_CONTENT_API } from '../../lib/helpers';

const BlogImg = ({ src, seoTitle }) => {
  const newSrc = src.replace(`${WP_CONTENT_API()}/uploads/`,"").
  replace(/https.*.wp.com/g, '').replace('/admin.automark.pk/wp-content/uploads/', '').
  replace(/http(s)?:\/\/www.automark.pk\/wp-content\/uploads/g,"")
  .replace(/\?.*/g, '');

  return  <picture>
    <source media="(max-width: 799px)" srcSet={`https://res.cloudinary.com/dwqo3od7a/image/upload/ar_16:9,w_480,c_fill,f_auto/wp-remote/${newSrc}`} />
    <source media="(min-width: 800px)" srcSet={`https://res.cloudinary.com/dwqo3od7a/image/upload/ar_16:9,w_1024,c_fill,f_auto/wp-remote/${newSrc}`} />
    <img src={`https://res.cloudinary.com/dwqo3od7a/image/upload/ar_16:9,w_1024,c_fill,f_auto/wp-remote/${newSrc}`} 
    alt={parse(seoTitle[1])} loading="lazy" />
  </picture>
}

export default BlogImg;