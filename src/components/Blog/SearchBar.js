import { Search } from 'react-feather';
import Link from 'next/link';

const SearchBar = () => (
    <div className="mb-4 md:flex md:mx-0 max-w-xs md:max-w-none md:w-1/2 mx-auto">
        <Link href="/search">
            <a className="flex">
                <Search color="#1c64f2" className="mr-1" />
                <span className="text-blue-600 underline">Search in Automark</span>
            </a>
        </Link>
    </div>
);

export default SearchBar;