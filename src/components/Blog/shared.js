/* mobile: 320 x 152 */
import Img from './Img';
const Image = ({ mobileImgUrl, postTitle }) => (
 <Img width={522} height={248} src={mobileImgUrl} alt={postTitle} />
);

export { Image };