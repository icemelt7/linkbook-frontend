import Link from "next/link";
import { DOMAIN } from '../../lib/helpers';
export const Label = ({ value, link = ""}) => {
  return (
    <div className="uppercase hidden absolute md:left-2 md:block font-sans bg-gray-900 text-white antialiased text-xss rounded top-2 px-3 h-6 leading-6 z-10">
      {link.trim() !== "" && <Link href={link.replace(DOMAIN, '')}>
        <a>{value}</a>
      </Link>}
    </div>
  )
}

export const Title = ({ children }) => {
  return (
    <>
      <p className="text-3xl font-bold leading-8 text-cool-gray-900 mb-3">
        {children}
      </p>
      <div className="border-b border-orange-500 w-8 h-1 mb-2"></div>
    </>
  );
};