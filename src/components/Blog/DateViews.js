import { useEffect } from 'react';
import { useQuery } from 'react-query';
import { useInView } from 'react-intersection-observer'
import formatDistanceToNow from 'date-fns/formatDistanceToNow';
import FireIcon from './FireIcon';

export const DateViews = ({ id, date, updateCount }) => {
  const [ref, inView] = useInView();
  const { data, isSuccess } = useQuery(id, getCount,
    { enabled: inView, staleTime: 1000 * 60 * 5 });

  useEffect(() => {
    if (updateCount) {
      setTimeout(() => {
        getAndUpdateView(id);
      }, 1600);
    }
  }, []);
  return (
    <div className="text-xs flex max-w-xs mb-4 items-center">
      <span ref={ref} className="text-gray-600 mr-2">
        {date && formatDistanceToNow(new Date(date), { addSuffix: true })}
      </span>
      <div className="flex items-center">
        {isSuccess && <>
          <FireIcon />
          <span className="text-red-500 font-bold">{data}
            <span className="hidden md:inline"> views</span></span>
        </>}
      </div>
    </div>
  )
}

export const Views = ({ id, updateCount }) => {
  const [ref, inView] = useInView();
  const { data } = useQuery(id, getCount, { enabled: inView, staleTime: 1000 * 60 * 5 });
  // const query = {
  //   data: null
  // };

  useEffect(() => {
    if (updateCount) {
      setTimeout(() => {
        getAndUpdateView(id);
      }, 1600);
    }
  }, []);
  return (
    <p ref={ref} className="text-red-500 font-sans antialiased text-xs mb-0 font-extrabold">
      {data} <span className="hidden lg:inline">Views</span>
    </p>
  );
};

const getCount = (id) => {
  const query = `query getPostCount($id: Int!) {
    wp_posts(where: {wp_post_id: {_eq: $id}}) {
      post_count
    }
  }`;
  const p = fetch(url, {
    method,
    headers,
    body: JSON.stringify({
      query,
      variables: { id },
    })
  });

  return new Promise(async (resolve, reject) => {
    try {
      const raw = await p;
      const { data } = await raw.json();
      resolve(data?.wp_posts?.[0]?.post_count);
    } catch (e) {
      reject(e);
    }
  })
}
const getAndUpdateView = (id) => {
  const query = `query getPostCount($id: Int!) {
    wp_posts(where: {wp_post_id: {_eq: $id}}) {
      post_count
    }
  }`;
  const p = fetch(url, {
    method,
    headers,
    body: JSON.stringify({
      query,
      variables: { id },
    })
  });

  return new Promise(async (resolve, reject) => {
    try {
      // Try to get the row
      const raw = await p;
      const { data } = await raw.json();

      // No row was found
      if (data.wp_posts.length === 0) {
        const newInsert = fetch(url, {
          method,
          headers,
          body: JSON.stringify({
            query: insertOne,
            variables: {
              id
            }
          })
        });

        // Insert new post.
        await newInsert;
        resolve(0);
        return;
      };
      // If there was a post increment count
      const incrementPromise = fetch(url, {
        method,
        headers,
        body: JSON.stringify({
          query: increment,
          variables: {
            wp_post_id: id
          }
        })
      });

      const newCountRaw = await incrementPromise;
      const { data: data2 } = await newCountRaw.json();
      resolve(data2.update_wp_posts.returning[0].post_count);
    } catch (e) {
      reject(e);
    }
  });
}

const url = 'https://graphql.automark.pk/v1/graphql';
const method = 'POST';
const headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
};
const insertOne = `mutation addPost($id: Int!) {
  insert_wp_posts_one(object: {wp_post_id: $id, post_count: 0}) {
    post_count
  }
}`

const increment = `mutation IncrementPostCount($wp_post_id: Int!) {
  update_wp_posts(where: {wp_post_id: {_eq: $wp_post_id}}, _inc: {post_count: 1}) {
    returning {
      post_count
    }
  }
}
`