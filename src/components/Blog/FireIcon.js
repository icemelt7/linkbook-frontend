const FireIcon = () => <img alt="views-icon" src="/static/fire-alt.svg" width="11" height="16" className="h-4 mr-1" />;
export default FireIcon;