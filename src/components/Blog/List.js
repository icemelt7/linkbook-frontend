import Head from 'next/head';
import parse from 'html-react-parser';
import ReactPaginate from 'react-paginate';
import { useRouter } from 'next/router';
import { PostList } from './PostsList';
import NavBar from './NavBar';
import FooterBlog from '../FooterBlog';
import SearchBar from './SearchBar';
import Breadcrumb from './Breadcrumb';
import { Title } from './Label';
import { getBreadCrumbsJson, generateBreadCrumbDataForList } from '../../lib/helpers';

const List = ({
  parentName,
  parentLink,
  rootslug,
  posts,
  categoryName,
  currentPage,
  display_type,
  totalPages,
  navBar,
  preferType
}) => {
  const router = useRouter();
  if (router.isFallback) {
    return <div>Loading</div>;
  }

  if (!rootslug) {
    rootslug = `category/${router.query.params[0]}`;
  }

  let taxonomy = rootslug;
  try {
    if (preferType && posts[0].type) {
      taxonomy = posts[0].type;
    } else {
      taxonomy = `${posts[0]._embedded['wp:term'][0][0].taxonomy}/${posts[0]._embedded['wp:term'][0][0].slug}`;
    }
  }catch(e){

  }
  const paginationHandler = ({ selected: page }) => {
    const actualPage = page + 1;
    router.push({
      pathname: `/${taxonomy}/page/${actualPage}`,
    });
  };

  const breadcrumbs = generateBreadCrumbDataForList({
    parentName,
    parentLink,
    categoryName
  });
  return (
    <>
      <Head>
        <meta name="robots" content="index, follow" />
        <title>All {categoryName} - Automark</title>
        {parse(getBreadCrumbsJson(breadcrumbs))}
      </Head>
      <NavBar navBar={navBar} />
      <div className="max-w-6xl mx-auto md:px-16 px-7">
        <div className="prose prose-md md:prose-2xl">
          <Breadcrumb breadcrumbs={breadcrumbs} />
        </div>
        <div className="md:max-w-none max-w-xs block mt-10 md:mx-0 mx-auto mb-4">
          <Title>All {categoryName}</Title>
        </div>
        <SearchBar />
        <div className="md:flex md:justify-between md:flex-wrap">
          <PostList
            rootslug={rootslug}
            categoryName={categoryName}
            posts={posts}
            isGrid={display_type === 'Grid' ? true : false}
          />
        </div>
        <ReactPaginate
          previousLabel={
            <svg className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
              <path
                fillRule="evenodd"
                d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                clipRule="evenodd"
              />
            </svg>
          }
          nextLabel={
            <svg className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
              <path
                fillRule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clipRule="evenodd"
              />
            </svg>
          }
          nextLinkClassName={
            '-ml-px relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-500 hover:text-gray-400 focus:z-10 focus:outline-none focus:border-blue-300 focus:ring-blue active:bg-gray-100 active:text-gray-500 transition ease-in-out duration-150'
          }
          previousLinkClassName={
            'relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-500 hover:text-gray-400 focus:z-10 focus:outline-none focus:border-blue-300 focus:ring-blue active:bg-gray-100 active:text-gray-500 transition ease-in-out duration-150'
          }
          breakLabel={'...'}
          breakClassName={
            '-ml-px relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-700'
          }
          activeLinkClassName={'bg-gray-100 text-blue-700'}
          containerClassName={'shadow-sm inline-flex'}
          subContainerClassName={'pages pagination'}
          pageLinkClassName={
            '-ml-px relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:z-10 focus:outline-none focus:border-blue-300 focus:ring-blue transition ease-in-out duration-150'
          }
          pageCount={parseInt(totalPages, 10)} //page count
          marginPagesDisplayed={2}
          pageRangeDisplayed={2}
          hrefBuilder={page => `/${taxonomy}/page/${page}`}
          initialPage={parseInt(currentPage - 1, 10)}
          onPageChange={paginationHandler}
          disableInitialCallback={true}
        />
      </div>
      <FooterBlog />
    </>
  );
};

export default List;
