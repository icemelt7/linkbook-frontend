import NextLink from 'next/link';
import React, { useState } from 'react';
import parse from 'html-react-parser';
import { hrefBasedOnObject, asBasedOnObject } from '../../lib/helpers';
import NavLink from './NavLink';

const NavBar = ({ navBar }) => {
  const [open, setOpen] = useState(false);

  let show = "";
  let bg = "";
  let textcolor = "";

  function handleClick() {
    setOpen(!open);
  }

  show = open ? "block" : "hidden";
  bg = open ? "bg-white" : "bg-red-700";
  textcolor = !open ? "text-white" : "text-red-700";

  return (
    <div>
      <div className="h-1 z-20 block bg-red-700 fixed w-full "></div>
      <div
        // style={{ borderBottom: "1px solid #eee" }}
        className={`sm:bg-white ${bg} md:block border-b border-t border-gray-200 flex items-center justify-between flex-wrap top-0 shadow-lg z-10`}
      >
        <div className="flex items-center w-full">
          <div className="block sm:hidden">
            <button
              className={`fill-current flex items-center px-3 py-2 rounded ${textcolor} sm:text-red-700`}
              onClick={handleClick}
            >
              <svg
                className="h-3 w-3"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <title>Menu</title>
                <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
              </svg>
            </button>
          </div>
          <NextLink href="/">
            <a style={{ left: 'calc(50% - 43px)'}} className="absolute text-xl text-white sm:hidden font-bold block">Automark</a>
          </NextLink>
        </div>

        <ul
          className={`w-full ${show} flex-grow sm:flex sm:items-center sm:w-autoflex flex-wrap justify-center font-sans sm:pb-0 pb-4`}
        >
          {navBar.items.map(item => <NavBarItem key={item.slug} item={item} />)}
        </ul>
      </div>
    </div>
  );
};

const NavBarItem = ({ children, ...props }) => {
  const { title, object, slug, url, child_items } = props.item;
  if (object === 'custom' && child_items?.length) {
    return <li key={title}>
      <button>{title}</button>

      <div className="absolute bg-white w-60 shadow-lg mt-5 border border-gray-200 rounded-sm ">
        <ul>
          {child_items.map(item => <NavBarItem item={item} />)}
        </ul>
      </div>
    </li>
  }
  return <li key={title} className="relative dropdown py-2 z-20 block sm:inline-block sm:mt-0 text-center text-sm md:text-base md:py-6">
    <Link
      href={hrefBasedOnObject(object, url, slug)}
      as={asBasedOnObject(object, url, slug)}
      prefetch={false}
    >
      <a>
        {parse(title)}
      </a>
    </Link>
  </li>
}
const Link = ({ children, ...props }) => {
  return <NavLink {...props} activeClassName="text-red-600">
    {React.cloneElement(children, { className: "mr-6 uppercase text-gray-800 font-thin hover:text-red-600" })}
  </NavLink>
}
export default NavBar;
