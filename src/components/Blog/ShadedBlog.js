import React from "react";
import format from "date-fns/format";
import { WP_CONTENT_API } from "../../lib/helpers";
import FireIcon from "./FireIcon";

const imgTransform = (w, h, postImgUrl) => `https://res.cloudinary.com/dwqo3od7a/image/upload/w_${w},h_${h},c_fill,r_20,f_auto/wp-remote/${postImgUrl.replace(
  `${WP_CONTENT_API()}/uploads/`,
  ""
)}`;

function ShadedBlog({ width, height, post }) {
  const {
    slug,
    excerpt: { rendered: summary },
    date,
    title: { rendered: postTitle },
    jetpack_featured_media_url: postImgUrl
  } = post;

  const imgUrl = imgTransform(width, height, postImgUrl);
  return (
    <div key={slug} className="relative mb-2">
      {/* <Link key={slug} href="/[blogslug]" as={`/${slug}`}> */}
      <Label value="Update" />
      {imgUrl && <Image imgUrl={imgUrl} postTitle={postTitle} />}
      <div
        className="absolute left-0 bottom-0 block h-full w-full object-contain rounded-b-md"
        style={{
          background: "linear-gradient(rgba(20, 31, 52, 0) 0%, #141f34 100%)",
          opacity: "0.8"
        }}
      />
      <Title value={postTitle} />
      <div className="absolute inset-x-0 bottom-0 px-6">
        <div className="flex mb-3 justify-between">
          <p className="font-sans antialiased text-xs leading-6 mb-0 text-white font-semibold">
            {format(new Date(date), "MMM d, yyyy")}
          </p>
          <div className="flex">
            <FireIcon />
            <Views count="28,000" />
          </div>
        </div>
      </div>
      {/* hello */}
    </div>
  );
}

const Title = ({ value }) => {
  return (
    <>
      <p className="clamped-two block antialiased text-lg font-bold leading-6 mb-10 absolute inset-x-0 bottom-0 px-6 text-white">
        {value}
      </p>
    </>
  );
};

const Label = ({ value }) => {
  return (
    <div className="uppercase absolute right-2 font-sans bg-gray-900 text-white antialiased text-xs rounded top-2 px-3 h-6 leading-6 z-10">
      {value}
    </div>
  );
};

const Image = ({ imgUrl, postTitle }) => (
  <img className="mb-2 relative" src={imgUrl} alt={postTitle} />
);

const Views = ({ count }) => {
  return (
    <p className=" text-red-600 font-sans antialiased text-xs leading-6 mb-0 font-extrabold">
      {count} Views
    </p>
  );
};
export default ShadedBlog;
