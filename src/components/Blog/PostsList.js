import NormalBlogs from "../NormalBlogs";
import format from "date-fns/format";
import Img from "./Img";
import Link from "next/link";
import { Label } from './Label';
import { itemLinkBasedOnCategory, WP_CONTENT_API } from "../../lib/helpers";
export const PostList = ({ rootslug, categoryName, posts, isGrid }) => (
  <div
    className={
      !isGrid
        ? "md:pr-4 md:px-0 md:max-w-screen-md"
        : "grid grid-cols-1 px-12 gap-8 sm:grid-cols-1 md:grid-cols-3 sm:mx-auto md:px-0"
    }
  >
    {posts.map(
      ({
        id,
        slug,
        date,
        title,
        taxonomy,
        name,
        jetpack_featured_media_url: postImgUrl,
        _embedded,
      }) => {
        const postTitle = title?.rendered || name;
        let _embeddedValues;
        let termName = categoryName;
        let termLink = `/${taxonomy}`;
        if (_embedded) {
          _embeddedValues = {
            "wp:term": [[{ link: termLink, name: termName }]],
          } = _embedded
        }
        if (!isGrid) {
          return (
            <NormalBlogs
              id={id}
              key={slug}
              slug={slug}
              postImgUrl={postImgUrl}
              postTitle={postTitle}
              date={date}
              termLink={termLink}
              termName={termName}
              categoryName={categoryName}
              rootslug={rootslug}
            />
          );
        } else {
          const imgUrl = (w, h) =>
            `https://res.cloudinary.com/dwqo3od7a/image/upload/w_${w},h_${h},c_fill,r_10,f_auto/wp-remote/${postImgUrl.replace(
              `${WP_CONTENT_API()}/uploads/`,
              ""
            )}`;
          return (
            <Link
              key={slug}
              href={itemLinkBasedOnCategory(rootslug || categoryName, slug).href}
              as={itemLinkBasedOnCategory(rootslug || categoryName, slug).as}
            > 
              <a>
                <div className="relative group max-w-sm bp:max-w-none sm:max-w-none rounded overflow-hidden mb-8 shadow-md hover:shadow-lg">
                  {postImgUrl && <Img
                    width={324}
                    height={160}
                    className="w-full"
                    src={imgUrl}
                    alt={postTitle}
                  />}
                  <Label link={termLink} value={termName} />
                  <div className="px-6 py-4 mt-4">
                    <div className="font-bold text-base text-center sm:text-left sm:text-xl mb-2 text-black group-hover:text-red-500 ">
                      {postTitle.includes("Monthly")
                        ? postTitle.substring(8, postTitle.length)
                        : postTitle}
                    </div>
                    <div className="font-sans font-thin text-xs text-gray-500 mb-2 text-center md:text-left">
                      {format(new Date(date), "MMM d, yyyy")}
                    </div>
                  </div>
                </div>
              </a>
            </Link>
          );
        }
      }
    )}
  </div>
);
