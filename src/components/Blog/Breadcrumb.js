import Link from "next/link";

const Breadcrumb = ({ breadcrumbs }) => {
  // console.log(breadcrumbs)
  return <small className="mb-1">
    {breadcrumbs.map((breadcrumb, i) => {
      const props = {
        href: breadcrumb.item,
        ...breadcrumb.as && {as: breadcrumb.as}
      }
      return <span key={i}>
        {breadcrumb.item ? <Link {...props} >
          <a>{breadcrumb.name}</a>
        </Link> : breadcrumb.name} 
        {i !== breadcrumbs.length - 1 && <span className="mx-2">&gt;</span>}
      </span>
    })}
  </small>
}

export default Breadcrumb;