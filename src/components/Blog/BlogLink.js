const BlogLink = ({ href, children }) => {
    return <a href={href} className="underline text-blue-600 py-1 font-medium">{children}</a>
}

export default BlogLink;