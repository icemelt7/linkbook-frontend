import Head from 'next/head';
import Link from 'next/link';
import parse, { domToReact } from 'html-react-parser';
import { useRouter } from 'next/router';
import NavBar from "./NavBar";
import Img from './Img';
import FooterBlog from "../FooterBlog";

import { WP_CONTENT_API, seoParseOptions, generateBreadCrumbDataForPost, getBreadCrumbsJson } from "../../lib/helpers";
import BlogLink from './BlogLink';
import BlogImg from './BlogImg';
import Breadcrumb from './Breadcrumb';

const Post = ({ children, parentLink, parentName, post, navBar }) => {
  const router = useRouter()
  if (router.isFallback) {
    return <div>Loading...</div>
  }
  if (!post) {
    return <></>;
  }
  const {
    content: { rendered: postContent },
    title: { rendered: postTitle },
    yoast_head,
    date,
    jetpack_featured_media_url: postImgUrl,
    _embedded: { 
      author: [authorToDisplay],
      'wp:term': [[term]]
    }
  } = post;
  const seoTitle = yoast_head.match(/<meta property=\"og:title\" content=\"(.*)\" \/>/)
  const finalImage = (w, h, mobile) => {
    if (mobile) {
      return `https://res.cloudinary.com/dwqo3od7a/image/upload/ar_16:9,w_480,c_fill,f_auto/wp-remote/${postImgUrl.replace(
        `${WP_CONTENT_API()}/uploads/`,
        ""
      )}`  
    }
    return `https://res.cloudinary.com/dwqo3od7a/image/upload/ar_16:9,w_1024,c_fill,f_auto/wp-remote/${postImgUrl.replace(
      `${WP_CONTENT_API()}/uploads/`,
      ""
    )}`
  };

  const galleryOptions = {
    replace: ({ name, attribs, children }) => {
      if (name === 'img') {
        return <BlogImg src={attribs.src} seoTitle={seoTitle} />
      }
      if (name === 'a') {
        return <div>{domToReact(children, galleryOptions)}</div>
      }
    }
  }
  const parseOptions = {
    replace: ({ name, attribs, children }) => {
      if (name === 'img' && attribs?.['data-src']) {
        const src = attribs?.['data-src'];
        return <BlogImg src={src} seoTitle={seoTitle} />;
      }
      if (name === 'a' && attribs?.style?.includes('background-image')) {
        const datasrc = attribs.style.match(/http.*.jpg/g)[0];
        return <BlogImg src={datasrc} seoTitle={seoTitle} />;
      }
      if (name === 'a') {
        const href = attribs?.href;
        if (href.includes('wp-content/uploads')) {
          return <BlogImg src={href} seoTitle={seoTitle} />;
        }
        return <BlogLink {...attribs} href={href}>{domToReact(children, parseOptions)}</BlogLink>;
      }

      if (name === 'figure') {
        return <p>{domToReact(children, parseOptions)}</p>;
      }

      if (name === 'video' && attribs?.['data-src']) {
        const src = attribs?.['data-src'];
        return <video src={src} controls />;
      }

      if (name === 'iframe') {
        return <iframe src={attribs['data-src']} className="w-full" style={{height: '540px'}} />
      }
      // sharedaddy
      if (name === 'div' && attribs?.class?.includes('sharedaddy')) {
        return <p />;
      }

      if (name === 'div' && attribs?.class?.includes('tiled-gallery')) {
        return <div>{domToReact(children, galleryOptions)}</div>;
      }
    }
  };
  const canonicalUrl = `https://www.automark.pk/${post.slug}`;

  const breadcrumbs = generateBreadCrumbDataForPost({
    term, 
    parentLink, 
    parentName, 
    title: parse(postTitle)
  });

  return (
    <>
      <Head>
        {parse(yoast_head, seoParseOptions)}
        <title>{parse(seoTitle[1])}</title>
        {parse(getBreadCrumbsJson(breadcrumbs))}
      </Head>
      <NavBar navBar={navBar} />
      {/* justify-center text-center items-center */}
      <article className="prose prose-md md:prose-2xl mx-auto mt-8 px-4 sm:px-0">
        <Breadcrumb breadcrumbs={breadcrumbs} />
        <h1>
          {parse(postTitle)}
        </h1>
        {postImgUrl && <Img
          className="shadow-lg"
          width={1920}
          height={1080}
          src={finalImage}
          alt={parse(seoTitle[1])}
        />}
        <div className="pt-serif pb-8 mt-8">
          {parse(postContent, parseOptions)}
          {children}
        </div>
        <small>Written by: </small>
        <p className="text-lg text-red-600">{authorToDisplay.name}</p>
        <p>{authorToDisplay.description}</p>
        <div>
          <Link href="/" prefetch={false}>
            <a className="text-2xl font-bold text-blue-600 underline">
              Checkout our website for more informative articles
            </a>
          </Link>
        </div>
        <div
          className="fb-like"
          data-share="true"
          data-width="450"
          data-show-faces="true">
        </div>
        <div id='discourse-comments'></div>

        <script type="text/javascript" dangerouslySetInnerHTML={{__html: `
          DiscourseEmbed = { discourseUrl: 'https://forum.automark.pk/',
                            discourseEmbedUrl: '${canonicalUrl}' };

          (function() {
            var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
            d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
          })();
          `}}>
        </script>
      </article>
      <FooterBlog />
    </>
  );
}

export default Post;