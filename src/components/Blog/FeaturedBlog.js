import parse from 'html-react-parser';
import formatDistanceToNow from 'date-fns/formatDistanceToNow';
import Link from "next/link";
import { imgTransform } from "../../lib/helpers";
import { Image } from "./shared";
import { Label } from "./Label";
import FireIcon from "./FireIcon";

function FeaturedBlog({ post, small, preload }) {
  if (!post?.slug) {
    return null;
  }
  const {
    slug,
    date,
    title: { rendered: postTitle },
    jetpack_featured_media_url: postImgUrl,
    _embedded: {
      "wp:term": [[{ link: categoryLink, name: categoryName }]],
    },
  } = post;

  const mobileImgUrl = (w, h, mobile) => {
    if (mobile) {
      return imgTransform(522 / 2, 248 / 2, postImgUrl);
    }
    return imgTransform(522, 248, postImgUrl);
  }
  return (
    <div key={slug} className="z-0 relative max-w-xs md:max-w-full mx-auto">
      {/* <Link key={slug} href="/[blogslug]" as={`/${slug}`}> */}
      <Label link={categoryLink} value={categoryName} />
      {postImgUrl ? (<Image
          mobileImgUrl={mobileImgUrl}
          postTitle={postTitle}
          preload={preload}
        />) : <div style={{
            paddingTop: 'calc(248px / 522px * 100%)'
          }}></div>}
      <div
        className="absolute left-0 bottom-0 block h-full w-full object-contain rounded-b-md"
        style={{
          background: "linear-gradient(rgba(20, 31, 52, 0) 54%, #000 100%)",
          opacity: "0.8",
        }}
      />
      <div className="absolute bottom-0 w-full p-2">
        <Title value={postTitle} small={small} slug={slug} />
        <div className="lg:px-6 px-2">
          <div className="flex lg:mb-3 justify-between">
            <p className="font-sans antialiased text-xs leading-6 mb-0 text-white font-semibold">
              {formatDistanceToNow(new Date(date), { addSuffix: true })}
            </p>
            <div className="flex">
              <FireIcon />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const Title = ({ value, small, slug }) => {
  if (small) {
    return (
      <Link key={slug} href="/[blogslug]" as={`/${slug}`} prefetch={false}>
        <a
          className="transparent-background lg:text-medium lg:max-w-md lg:leading-8 lg:p-1 lg:mb-3 clamped-two block antialiased
      font-bold leading-tight text-md mb-1 px-2 text-white"
        >{parse(value)}</a>
      </Link>
    );
  }
  return (
    <Link key={slug} href="/[blogslug]" as={`/${slug}`} prefetch={false}>
      <a
        className="transparent-background lg:text-3xl lg:max-w-xl lg:leading-9 lg:p-1 lg:mb-3 clamped-two block antialiased text-md font-bold mb-1 leading-tight px-2 text-white"
      >{parse(value)}</a>
    </Link>
  );
};

export default FeaturedBlog;
