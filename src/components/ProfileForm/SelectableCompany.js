import React, { useState } from 'react';
import AsyncCreatableSelect from 'react-select/async-creatable';
import { createCompanyRemote } from '../../lib/API';
const sanityClient = require('@sanity/client');
const client = sanityClient({
  projectId: '5gyrk3u0',
  dataset: 'production',
  useCdn: true // `false` if you want to ensure fresh data
});

const SelectableCompany = ({ onChange, value, name }) => {
  const [status, setStatus] = useState('normal');
  if (status === 'creation-in-process') {
    return <div
      className="appearance-none block w-full bg-gray-200 text-gray-700 rounded py-3 px-4 mb-3"
    >Adding company please wait</div>
  }
  return <AsyncCreatableSelect
    key={value._id}
    isClearable
    isDisabled={status === 'creation-in-process'}
    loadOptions={getCompanies}
    onChange={(newValue, actionMeta) => handleChange({ newValue, actionMeta, onChange, name})}
    defaultValue={{label: value.name, value: value._id}}
    onCreateOption={(inputValue) => createCompany(inputValue, setStatus, onChange, name)}
  />
}

const handleChange = ({ newValue, actionMeta, onChange, name } ) => {
  const action = actionMeta.action;

  if (action === "select-option") {
    onChange({
      target: {
        name,
        value: {
          name: newValue.label,
          _id: newValue.value
        }
      }
    });
  }
};

const getCompanies = async (inputValue) => {
  const query = `*[_type == "company" && name match "${inputValue.toLowerCase()}*"]`;
  // Logger.log(query)
  const companies = await client.fetch(query);
  // Logger.log(profiles)
  return companies.map(company => {
    return {
      label: company.name,
      value: company._id
    }
  });
}

const createCompany = async (inputValue, setStatus, onChange, name) => {
  setStatus('creation-in-process');
  const result = await createCompanyRemote(inputValue);
  onChange({
    target: {
      name,
      value: {
        name: result.name,
        _id: result._id
      }
    }
  });
  setStatus('creation-complete');
}

export default SelectableCompany;
