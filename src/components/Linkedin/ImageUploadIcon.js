// Another way to import. This is recommended to reduce bundle size
import CircleLoader from "react-spinners/CircleLoader";
const ImageUploadIcon = ({ picUrl, linkedin_status, onClick}) => {
  if (linkedin_status === "loading_start") {
    return <CircleLoader loading={true} />
  }
  
  return picUrl == null ? (
    <div
      className="w-24 h-24 rounded-full mr-4 bg-gray-500 flex items-center text-center"
      onClick={onClick}
      id="upload_widget"
    >
      <p className="text-xs text-gray-800 cursor-pointer">
        Click here to upload
      </p>
    </div>
  ) : (
    <img
      className="w-24 h-24 rounded-full mr-4 shadow-md"
      src={picUrl}
      onClick={onClick}
      id="upload_widget"
    />
  )
}

export default ImageUploadIcon;