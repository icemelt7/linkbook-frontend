import React, { useContext } from "react";

import LinkedIn from "../../lib/LinkedIn";
import { UserContext } from "../../../pages/dashboard";
import { isServer } from "../../lib/firebase";
const LinkedInPage = ({ onChange, dispatch }) => {
  const userId = useContext(UserContext);
  const handleSuccess = data => {
    fetch(`${window.location.origin}/api/linkedin`, {
      method: "POST",
      body: JSON.stringify({
        code: data.code,
        userId
      })
    }).then(raw => {
      raw.json().then(({picurl}) => {
        onChange({
          target: {
            name: "picture",
            value: picurl
          }
        });

        dispatch({
          type: 'linkedin_loading_finish'
        })    
      });
    });
  };

  const handleFailure = error => {};

  const onClick = (e) => {
    dispatch({
      type: 'linkedin_loading_start'
    })
  }

  return (
    <div>
      <LinkedIn
        clientId="81fd49jjbco0tf"
        onSuccess={handleSuccess}
        onFailure={handleFailure}
        onClick={onClick}
        redirectUri={`${
          isServer() ? "https://stage.automark.pk" : window.location.origin
        }/linkedin`}
        scope="r_liteprofile r_emailaddress w_member_social"
      >
        <img
          src="/static/images/linkedin.png"
          alt="Get Profile Picture from LinkedIn"
          style={{ maxWidth: "180px" }}
        />
      </LinkedIn>
    </div>
  );
};

export default LinkedInPage;
