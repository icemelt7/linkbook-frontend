import format from 'date-fns/format';
import FireIcon from './Blog/FireIcon.js';

function ShadedBlog({ slug, postImgUrl, imgUrl, postTitle, date }) {
  return (
    <div key={slug} className="relative mb-2">
      {/* <Link key={slug} href="/[blogslug]" as={`/${slug}`}> */}
      <Label value="Update" />
      {postImgUrl && <Image imgUrl={imgUrl} postTitle={postTitle} />}
      <div className="absolute left-0 bottom-0 block h-full w-full object-contain rounded-b-md" style={{ background: "linear-gradient(rgba(20, 31, 52, 0) 0%, #141f34 100%)", opacity: "0.8" }} />
      <Title value={postTitle} />
      <div className="absolute inset-x-0 bottom-0 px-6">
        <div className="flex mb-3 justify-between">
          <p className="font-sans antialiased text-xs leading-6 mb-0 text-white font-semibold">{format(new Date(date), 'MMM d, yyyy')}</p>
          <div className="flex">
            <FireIcon />
            <Views count="28,000" />
          </div>
        </div>
      </div>
    </div>
  )
}

const Title = ({ value }) => {
  return (
    <>
      <p className="clamped-two block antialiased text-lg font-bold leading-6 mb-10 absolute inset-x-0 bottom-0 px-6 text-white">
        {value}
      </p>

    </>
  )
}

const Label = ({ value }) => {
  return (
    <div className="uppercase absolute right-2 font-sans bg-gray-900 text-white antialiased text-xs rounded top-2 px-3 h-6 leading-6 z-10">{value}</div>
  )
}

const Image = ({ imgUrl, postTitle }) => {
  return (
    <img className="mb-2 relative"
      srcSet={`${imgUrl(376, 250)},
                           ${imgUrl(320 * 2, 212 * 2)} 320w,
                           ${imgUrl(376 * 1.5, 250 * 1.5)} 1.5x,
                           ${imgUrl(376 * 2, 180 * 2)} 2x`}
      src={imgUrl(376 * 2, 250 * 2)} alt={postTitle} />
  )
}

const Views = ({ count }) => {
  return (
    <p className=" text-red-600 font-sans antialiased text-xs leading-6 mb-0 font-extrabold">{count} Views</p>
  )
}
export default ShadedBlogs;