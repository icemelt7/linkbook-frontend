import Router from 'next/router';






// 
// 
// 
// 



export const mainListItems = (slug) => (
  <div>
    <ListItem style={{marginBottom: '12px'}} onClick={() => Router.push('/dashboard')} button>
      <ListItemIcon>
        <DashboardIcon />
      </ListItemIcon>
      <ListItemText primary="Manage Profile" />
    </ListItem>
    <Button variant="contained" color="primary" style={{marginLeft: '48px'}}
    onClick={() => Router.push(`/profile/${slug}`)}>
      View My Profile
    </Button>
  </div>
);

export const secondaryListItems = (
  <div>
    <ListSubheader inset>Saved reports</ListSubheader>
    <ListItem button>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Current month" />
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Last quarter" />
    </ListItem>
    <ListItem button>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Year-end sale" />
    </ListItem>
  </div>
);
