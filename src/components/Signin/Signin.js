import React, { useState } from "react";
import { signInWithFirebase } from "../../lib/firebase";
import { Logger } from "../../lib/Logger";

function SignIn() {
  const [email, setEmail] = useState("");
  const submitForm = e => {
    e.preventDefault();
    signInWithFirebase({ email })
      .then(() => {
        let domain = email.split('@')[1]; // icemelt7@gmail.com gmail.com
        domain = `mail.${(domain === 'gmail.com') ? 'google.com' : domain}`;
        window.location.href = `/openlink?mailink=${domain}`;
      })
      .catch(error => Logger.log(error));
  };
  return (
    <div className="w-full h-screen flex items-center justify-center my-auto bg-gray-200">
      <form noValidate onSubmit={submitForm} className="bg-white shadow-md rounded px-10 pt-6 pb-10 w-1/3 mb-4 flex flex-col">
      <div class="mb-4">
        <label class="block font-bold text-2xl mb-2 text-center" for="email">
          Sign in
      </label>
        <input class="shadow appearance-none border rounded w-full py-2 px-3 mb-2 text-grey-darker" type="text" placeholder="Email"
        required
        value={email}
        onChange={e => {
          setEmail(e.target.value);
        }}
        id="email"
        label="Email Address"
        name="email" />
      </div>
      <div class=" text-center">
        <button class="bg-red-500 hover:bg-red-600 text-white font-bold py-2 px-4 rounded text-center w-full" type="submit">
          Send link to email 
      </button>
      </div>
    </form>
    </div>
  );
}

export default SignIn;
