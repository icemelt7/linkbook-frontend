import React from "react";

const MobileScreenHeader = ({ children }) => {
  return (
    <div className="bg-gray-100 p-4 flex border-gray-600 sm:items-center sm:rounded-tl-lg sm:rounded-tr-lg">
      {children}
    </div>
  );
};

export default MobileScreenHeader;
