import React from 'react';

const Title = ({ children }) => (
  <div className="flex-grow sm:justify-left justify-center">
    <p className="text-center text-xl sm:text-2xl tracking-normal -mr-16 sm:mr-0 align-middle">{children}</p>
  </div>
);

export default Title;
