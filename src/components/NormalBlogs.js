import Link from 'next/link';
import parse, { domToReact } from 'html-react-parser';
import fromUnixTime from 'date-fns/fromUnixTime';
import { Label } from './Blog/Label';
import { WP_CONTENT_API, DOMAIN, string_to_slug, itemLinkBasedOnCategory } from '../lib/helpers';
import Img from './Blog/Img';

function NormalBlogs({ rootslug, id, slug, postImgUrl, postTitle, date, categoryName, termLink, termName }) {
  return (
    <div key={slug} className="z-0 relative md:flex mb-12 bp:mb-4">
      <Label link={termLink} value={termName} />

      {postImgUrl ? (
        <Image
          postTitle={postTitle}
          postImgUrl={postImgUrl}
          href={itemLinkBasedOnCategory(rootslug || categoryName, slug).href}
          as={itemLinkBasedOnCategory(rootslug || categoryName, slug).as}
        />
      ) : (
        <div style={{ width: '278px' }}></div>
      )}

      <div
        className="
          md:pl-6
          md:px-0 
          md:flex 
          md:flex-wrap 
          md:flex-col
          md:content-between 
          md:justify-between
          md:mx-0  
          md:max-w-full 
          max-w-xs 
          md:max-w-none 
          mx-auto
          md:py-6
        "
      >
        <Title 
          rootslug={rootslug} 
          postTitle={postTitle} 
          categoryName={categoryName} 
          slug={slug} 
        />
      </div>
    </div>
  );
}

export const AlgoliaNormalBlog = ({ hit }) => {
  // console.log(hit)
  const {
    _highlightResult: {
      post_title: { value: postTitle },
    },
    post_date: date,
    post_type: rootslug,
    taxonomies,
    images,
    post_id: id,
    permalink: slug,
  } = hit;
  const taxonomy = Object.keys(taxonomies)[0];
  const termName = taxonomies?.[taxonomy]?.[0];

  const postImgUrl = images?.thumbnail?.url;
  const termLink = termName ? `/${taxonomy}/${string_to_slug(termName, '-')}` : '';
  return (
    <NormalBlogs
      id={id}
      key={slug}
      slug={slug.replace(`${DOMAIN}/`, '')}
      postImgUrl={postImgUrl}
      postTitle={postTitle}
      date={date}
      termLink={termLink}
      termName={termName}
      categoryName={termName}
      rootslug={rootslug}
      date={fromUnixTime(date)}
    />
  );
};
const Title = ({ rootslug, categoryName, postTitle, slug }) => {
  const parseOptions = {
    replace: ({ name, attribs, children }) => {
      if (name === 'ais-highlight-0000000000') {
        return <span className="bg-yellow-200">{domToReact(children)}</span>;
      }
    },
  };
  return (
    <div className="md:mx-0 md:mb-0">
      <Link
        key={slug}
        href={itemLinkBasedOnCategory(rootslug || categoryName, slug).href}
        as={itemLinkBasedOnCategory(rootslug || categoryName, slug).as}
        prefetch={false}
      >
        <a className="antialiased text-lg font-bold leading-6 bp:leading-7">
          {parse(postTitle, parseOptions)}
        </a>
      </Link>
    </div>
  );
};

const Image = ({ postTitle, postImgUrl, href, as }) => {
  const imgUrl = (w, h) =>
    `https://res.cloudinary.com/dwqo3od7a/image/upload/w_${w},h_${h},c_fill,r_10,f_auto/wp-remote/${postImgUrl.replace(
      `${WP_CONTENT_API()}/uploads/`,
      '',
    )}`;
  return (
    <Img
      href={href}
      as={as}
      width={276}
      height={175}
      className="mx-auto max-w-xs md:max-w-none md:mx-0 md:mb-0"
      src={imgUrl}
      alt={postTitle}
    />
  );
};
export default NormalBlogs;
