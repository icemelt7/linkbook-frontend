






import { useState } from 'react';
import { Logger } from '../lib/Logger';

function NestedNav({current, classes}) {
  // const classes = useStyles();
  const [open, setOpen] = useState(false);

  function handleClick() {
    setOpen(!open);
  }
  Logger.log(current + "asdasdas");
  
  return (
    <List
      component="nav"
      className={classes.root}
    >
      <ListItem button onClick={handleClick}>
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          {current === "loaded" ? 
          <ListItem button className={classes.nested}>
            <ListItemText primary="Hello" />
            <ListItemText primary="Hello" />
            <ListItemText primary="Hello" />
          </ListItem> :
          <ListItem button className={classes.nested}>
          <ListItemText primary="Hi" />
          <ListItemText primary="Hi" />
          <ListItemText primary="Hi" />
        </ListItem>
          }
        </List>
      </Collapse>
    </List>
  );
}
export default NestedNav;