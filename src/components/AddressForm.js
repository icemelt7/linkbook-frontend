import React, { useEffect, useRef } from "react";
import { storageRef } from "../lib/firebase";
import { listingThumbForProfilePic } from "../constants";
import { Logger } from "../lib/Logger";
import ImageUploadIcon from "./Linkedin/ImageUploadIcon";
import SelectableCompany from "./ProfileForm/SelectableCompany";

function AddressForm({ values, onChange, formStatus, saveInfo }) {
  const uploadFile = e => {
    Logger.log(e.target.files[0]);
    storageRef.child("cv/" + e.target.files[0].name).put(e.target.files[0]);
  };
  let myWidget = useRef(null);
  useEffect(() => {
    myWidget.current = cloudinary.createUploadWidget(
      {
        cloudName: "dwqo3od7a",
        uploadPreset: "r8rdgmtc"
      },
      (error, result) => {
        if (!error && result && result.event === "success") {
          Logger.log("Done! Here is the image info: ", result.info);
          onChange({
            target: {
              name: "picture",
              value: result.info.path
            }
          });
        }
      }
    );
  }, []);

  // Lot of mehnat to see if pic is changing or not
  let prevPic = useRef(null);
  useEffect(() => {
    if (!prevPic.current) {
      prevPic.current = values.picture;
    }
    if (prevPic.current !== values.picture) {
      saveInfo();
    }
  }, [values.picture]);
  // Pic work finish

  const uploadPictureOnClick = () => {
    myWidget.current.open();
  };

  const picUrl = values.picture
    ? listingThumbForProfilePic(values.picture)
    : null;
  return (
    <div>
      <div>
        <p className="text-base text-center py-2">
          Upload or Edit your picture
        </p>
        <div className="flex items-center mb-3 justify-center">
          <ImageUploadIcon
            picUrl={picUrl}
            linkedin_status={values.linkedin_status}
            onClick={uploadPictureOnClick}
          />
        </div>
      </div>

      <form className="w-full">
        <div className="mb-6">
          <div className="w-full mb-6">
            <label className="block uppercase text-gray-700 text-xs font-bold mb-2">
              Email
            </label>
            <input
              className="appearance-none block w-full bg-gray-300 text-gray-700 border border-gray-500 rounded py-3 px-4 mb-3 invalid focus:outline-none focus:bg-white"
              type="email"
              placeholder="email"
              id="email"
              name="email"
              disabled
              value={values.email}
            />
            {/* <p className="text-red-500 text-xs italic">Please fill out this field.</p> */}
          </div>
          <div className="w-full mb-6">
            <label className="block uppercase text-gray-700 text-xs font-bold mb-2">
              Full Name *
            </label>
            <input
              className="appearance-none block w-full bg-white text-gray-700 border border-gray-500 rounded py-3 px-4 mb-3 invalid focus:outline-none focus:bg-white"
              type="text"
              placeholder="Full name"
              required
              id="fullName"
              name="name"
              onChange={onChange}
              value={values.name}
            />
            {/* <p className="text-red-500 text-xs italic">Please fill out this field.</p> */}
          </div>
          <div className="w-full mb-6">
            <label className="block uppercase text-gray-700 text-xs font-bold mb-2">
              Phone number *
            </label>
            <input
              className="appearance-none block w-full bg-white text-gray-700 border border-gray-500 rounded py-3 px-4 mb-3 invalid focus:outline-none focus:bg-white"
              type="tel"
              placeholder="Phone number"
              required
              id="tel"
              name="phone_number"
              onChange={onChange}
              value={values.phone_number}
            />
            {/* <p className="text-red-500 text-xs italic">Please fill out this field.</p> */}
          </div>
          <div className="w-full mb-6">
            <label className="block uppercase text-gray-700 text-xs font-bold mb-2">
              Your City / Town
            </label>
            <input
              className="appearance-none block w-full bg-white text-gray-700 border border-gray-500 rounded py-3 px-4 mb-3 invalid focus:outline-none focus:bg-white"
              type="text"
              placeholder="Your City / Town"
              id="location"
              name="location"
              onChange={onChange}
              value={values.location}
            />
            {/* <p className="text-red-500 text-xs italic">Please fill out this field.</p> */}
          </div>
          <div className="w-full mb-6">
            <label className="block uppercase text-gray-700 text-xs font-bold mb-2">
              One line about you
            </label>
            <input
              className="appearance-none block w-full bg-white text-gray-700 border border-gray-500 rounded py-3 px-4 mb-3 invalid focus:outline-none focus:bg-white"
              type="text"
              placeholder="One line about you"
              id="oneline"
              name="headline"
              onChange={onChange}
              value={values.headline}
            />
            {/* <p className="text-red-500 text-xs italic">Please fill out this field.</p> */}
          </div>
          <div className="w-full mb-6">
            <label className="block uppercase text-gray-700 text-xs font-bold mb-2">
              Current Company
            </label>
            {/* className="appearance-none block w-full bg-white text-gray-700 border border-gray-500 rounded py-3 px-4 mb-3 invalid focus:outline-none focus:bg-white" */}
              
            <SelectableCompany
              type="text"
              placeholder="Current Company"
              id="currentCompany"
              name="current_job_company_beta"
              onChange={onChange}
              value={values.current_job_company_beta}
            />
            {/* <p className="text-red-500 text-xs italic">Please fill out this field.</p> */}
          </div>
          <div className="w-full mb-6">
            <label className="block uppercase text-gray-700 text-xs font-bold mb-2">
              Current Job Title
            </label>
            <input
              className="appearance-none block w-full bg-white text-gray-700 border border-gray-500 rounded py-3 px-4 mb-3 invalid focus:outline-none focus:bg-white"
              type="text"
              placeholder="Current Job Title"
              id="currentJob"
              name="current_job_desc"
              onChange={onChange}
              value={values.current_job_desc}
            />
            {/* <p className="text-red-500 text-xs italic">Please fill out this field.</p> */}
          </div>
          <div className="w-full mb-6">
            <label className="block uppercase text-gray-700 text-xs font-bold mb-2">
              Current Department
            </label>
            <input
              className="appearance-none block w-full bg-white text-gray-700 border border-gray-500 rounded py-3 px-4 mb-3 invalid focus:outline-none focus:bg-white"
              type="text"
              placeholder="Current Department"
              id="department"
              name="current_job_department"
              onChange={onChange}
              value={values.current_job_department}
            />
            {/* <p className="text-red-500 text-xs italic">Please fill out this field.</p> */}
          </div>
          <div className="w-full mb-6">
            <label className="block uppercase text-gray-700 text-xs font-bold mb-2">
              Industry
            </label>
            <input
              className="appearance-none block w-full bg-white text-gray-700 border border-gray-500 rounded py-3 px-4 mb-3 invalid focus:outline-none focus:bg-white"
              type="text"
              placeholder="Industry"
              id="industry"
              name="industry"
              onChange={onChange}
              value={values.industry}
            />
            {/* <p className="text-red-500 text-xs italic">Please fill out this field.</p> */}
          </div>
          <div className="w-full">
            <label className="block uppercase text-gray-700 text-xs font-bold mb-2">
              Sector
            </label>
            {/* <p className="text-red-500 text-xs italic">Please fill out this field.</p> */}
          </div>
          <div className="inline-block relative w-full mb-6">
            <select
              name="sector"
              value={values.sector}
              onChange={onChange}
              className="block appearance-none w-full bg-white border border-gray-600 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:ring"
            >
              <option value="sales">Sales</option>
              <option value="manufacturing">Manufacturing</option>
              <option value="Bike">Bike</option>
            </select>
            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
              <svg
                className="fill-current h-4 w-4"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
              >
                <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
              </svg>
            </div>
          </div>
          <div className="w-full">
            <label className="block uppercase text-gray-700 text-xs font-bold mb-2">
              Employed?
            </label>
            {/* <p className="text-red-500 text-xs italic">Please fill out this field.</p> */}
          </div>
          <div className="mb-6">
            <div className="custom-control custom-radio pb-3 ">
              <input
                type="radio"
                className="custom-control-input"
                id="customRadio"
                name="employed"
                value="true"
                onChange={onChange}
              />
              <label className="custom-control-label" for="customRadio">
                Yes
              </label>
            </div>

            <div className="custom-control custom-radio">
              <input
                type="radio"
                defaultChecked
                className="custom-control-input"
                id="defaultRadio"
                name="employed"
                value="No"
                onChange={onChange}
              />
              <label className="custom-control-label" for="defaultRadio">
                No
              </label>
            </div>
          </div>
        </div>
        <div className="w-full mb-6">
          <label className="block uppercase text-gray-700 text-xs font-bold mb-2">
            Interested in
          </label>
          <input
            className="appearance-none block w-full bg-white text-gray-700 border border-gray-500 rounded py-3 px-4 mb-3 invalid focus:outline-none focus:bg-white"
            type="text"
            id="industry"
            name="interested_in"
            onChange={onChange}
            value={values.interested_in}
          />
          <p className="text-gray-700 text-xs">Which field would you like to move in? Leave blank if happy with current field</p>
        </div>
        <div className="w-full mb-6">
          <label className="block uppercase text-gray-700 text-xs font-bold mb-2">
            Experience
          </label>
          <input
            className="appearance-none block w-full bg-white text-gray-700 border border-gray-500 rounded py-3 px-4 mb-3 invalid focus:outline-none focus:bg-white"
            type="number"
            min="0"
            placeholder="Experience"
            id="industry"
            name="experience"
            label="interested in"
            onChange={onChange}
            value={values.experience}
          />
          {/* <p className="text-red-500 text-xs italic">Please fill out this field.</p> */}
        </div>
        <div className="w-full mb-6">
          <label className="block uppercase text-gray-700 text-xs font-bold mb-2">
            Any other information about you
          </label>
          <textarea
            className="appearance-none block w-full bg-white text-gray-700 border border-gray-500 rounded py-3 px-4 mb-3 invalid focus:outline-none focus:bg-white"
            type="text"
            placeholder="Any other information about you"
            id="addedInfo"
            name="summary"
            rows="4"
            onChange={onChange}
            value={values.summary}
          />
          {/* <p className="text-red-500 text-xs italic">Please fill out this field.</p> */}
        </div>
      </form>
      <div className="relative overflow-hidden inline-block mb-4 text-center justify-center w-full">
        <label className="bg-blue-500 hover:bg-blue-700 text-white py-2 px-4 rounded mb-0">
          <input
            type="file"
            className="absolute"
            name="myfile"
            style={{ zIndex: "-1" }}
            onChange={uploadFile}
          />
          <span>Upload a file</span>
        </label>
        {/* <label className="block uppercase text-gray-700 text-xs font-bold mb-2">
          Upload A file
            </label> */}
      </div>

      <div className="w-full pb-6 flex justify-center text-center">
        {formStatus === "saving" ? (
          <p className="text-base text-red-600">Loading</p>
        ) : (
          <button
            className="bg-red-500 hover:bg-red-600 text-white py-2 px-4 rounded"
            onClick={saveInfo}
          >
            Save
          </button>
        )}
        <div className="pl-2 flex items-center text-center">
          {formStatus === "saved" && (
            <p className="text-xs text-red-600">Saved</p>
          )}
        </div>
      </div>
    </div>
  );
}

export default AddressForm;
