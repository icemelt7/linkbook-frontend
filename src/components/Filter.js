import React, { useState } from "react";
import FilterSearchButton from "./ProfileList/FilterSearchButton";
import BackButton from "./ProfileList/BackButton";
import EmployedToggle from "./ProfileList/EmployeeToggle";
import SearchBox from "./ProfileList/SearchBox";
import CollapsableBox from "./ProfileList/CollapsableBox";
import ClearRefinements from "./ProfileList/ClearRefinements";
import NumericMenu from "./ProfileList/NumericMenu";
import MobileScreenHeader from "./Mobile/MobileScreenHeader";
import Title from "./Mobile/Title";

const Filter = ({ INSTANT_MODE, forceClearRefine }) => {
  const [open, setOpen] = useState(false);
  const toggleFilter = e => {
    e.preventDefault();
    setOpen(!open);
  };

  return (
    <div className="mb-4 sm:mt-4 bg-white rounded-lg sm:ml-4 ml-0 sm:sticky top-0 sm:shadow-lg sm:border sm:border-gray-600" style={{ top: "63px" }}>
      <div className="sm:hidden block">
        <SearchBox INSTANT_MODE={INSTANT_MODE} />
      </div>
      <div className="sm:hidden block topBar">
        <FilterSearchButton toggleFilter={toggleFilter} />
        <CollapsableBox open={open}>
          <MobileScreenHeader>
            <BackButton toggleFilter={toggleFilter} />
            <Title>Filters</Title>
            <ClearRefinements
              forceClearRefine={forceClearRefine}
              clearsQuery
            />
          </MobileScreenHeader>

          <EmployedToggle attribute="employed" label="Employed" value={true} />
          <NumericMenu
            attribute="yearsOfExp"
            items={[
              { label: "Less than 2 Years", end: 2 },
              { label: "3 to 5 Years", start: 3, end: 5 },
              { label: "5 to Senior", start: 5, end: 500 }
            ]}
          />
        </CollapsableBox>
      </div>
      <div className="sm:block hidden sideBar">
        <MobileScreenHeader>
          <BackButton toggleFilter={toggleFilter} />
          <Title>Filters</Title>
        </MobileScreenHeader>
        <EmployedToggle attribute="employed" label="Employed" value={true} />
        <NumericMenu
          attribute="yearsOfExp"
          items={[
            { label: "Less than 2 Years", end: 2 },
            { label: "3 to 5 Years", start: 3, end: 5 },
            { label: "5 to Senior", start: 5, end: 500 }
          ]}
        />
      </div>
      <div className="sm:block hidden">
        <SearchBox INSTANT_MODE={INSTANT_MODE} />
        <ClearRefinements
            forceClearRefine={forceClearRefine}
            clearsQuery
          />
      </div>
    </div>
  );
};

export default Filter;
