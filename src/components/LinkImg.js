import PropTypes from "prop-types";

import styles from "./Img.module.css";

export default function Img(props) {
    const { src, alt, width, height, ...rest } = props;
    return (
        <div className={`${styles.root} mx-auto md:mx-0`} style={{ "--maxWidth": width }}>
            <div
                className={styles.wrapper}
                style={{
                    "--height": height,
                    "--width": width
                }}
            >
              <Link>
                <img
                    src={src}
                    loading="lazy"
                    alt={alt}
                    width={width}
                    height={height}
                    className={`${styles.img} ${rest.className}`}
                />
              </Link>
            </div>
        </div>
    );
}

Img.propTypes = {
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    src: PropTypes.string.isRequired,
    alt: PropTypes.string.isRequired
};