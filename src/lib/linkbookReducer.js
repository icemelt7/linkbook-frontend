import { useReducer } from 'react';
import reducer from './reducer';
import { userProfileFields } from '../constants';

function useLinkbookReducer() {
  const emptyUserFields = userProfileFields.reduce((prev, current) => {
    return { ...prev, [current]: "" };
  }, {});
  emptyUserFields.current = "init";
  emptyUserFields.linkedin_status = 'no_linkedin'
  const [values, dispatch] = useReducer(reducer, emptyUserFields);
  return [values, dispatch];
}

export default useLinkbookReducer;