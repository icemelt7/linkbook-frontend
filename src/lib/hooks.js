import { useEffect } from 'react';
// import LogRocket from 'logrocket';
import { firebase, getUser } from './firebase';
const useFirebaseUser = (dispatch) => {
  useEffect(() => {
    firebase.auth().onAuthStateChanged(async function(user) {
      if (user) {
        // This is an example script - don't forget to change it!
        // LogRocket.identify(user.uid, {
        //   name: user.name,
        //   email: user.email
        // });
        const moreUserInfo = await getUser(user.email);
        dispatch({
          type: "setUser",
          payload: {
            ...user, 
            ...moreUserInfo
          }
        });
      } else {
        dispatch({
          type: "userDoesNotExit"
        });
      }
    });
  }, []);
}

export { useFirebaseUser }