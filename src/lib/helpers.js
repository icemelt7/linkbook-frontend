export const string_to_slug = (str, separator) => {
  str = str.trim();
  str = str.toLowerCase();

  // remove accents, swap ñ for n, etc
  const from = "åàáãäâèéëêìíïîòóöôùúüûñç·/_:; ";
  const to = "aaaaaaeeeeiiiioooouuuunc------";

  for (let i = 0, l = from.length; i < l; i++) {
    str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
  }

  return str
    .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
    .replace(/\s+/g, "-") // collapse whitespace and replace by -
    .replace(/-+/g, "-") // collapse dashes
    .replace(/^-+/, "") // trim - from start of text
    .replace(/-+$/, "") // trim - from end of text
    .replace(/-/g, separator);
}

export const isAvail = str => {

  if (str === "") {
    str = "Information not Available";
  }
  return str;
};

export const isUpperCase = str => {
  isAvail(str);
  if (!str) {
    return "";
  }
  if (str === str.toUpperCase()) {
    str = str
      .toLowerCase()
      .split(" ")
      .map(s => s.charAt(0).toUpperCase() + s.substring(1))
      .join(" ");
  } else if (str === str.toLowerCase()) {
    str = str
      .split(" ")
      .map(s => s.charAt(0).toUpperCase() + s.substring(1))
      .join(" ");
  }
  return str;
};

export const imgTransform = (w, h, postImgUrl) => {
  if (!postImgUrl) {
    return null;
  }
  return `https://res.cloudinary.com/dwqo3od7a/image/upload/w_${w},h_${h},c_fill,r_5,f_auto/wp-remote/${postImgUrl.replace(
    /http(s)?:\/\/admin\.automark\.pk\/wp-content\/uploads\//g,
    ""
  )}`
}

export const seoParseOptions = {
  replace: ({ name, attribs }) => {
    if (name === 'link' && attribs.rel === 'canonical') {
      return <link {...attribs} href={attribs.href.replace(LEGACY_DOMAIN, 'https://www.automark.pk').slice(0,-1)} />
    }
    if (name === 'meta' && attribs.property === 'og:url') {
      return <meta {...attribs} content={attribs.content.replace(LEGACY_DOMAIN, 'https://www.automark.pk').slice(0,-1)} />
    }
    if (name === 'meta' && attribs.property === 'og:description') {
      return <>
        <meta content={attribs.content} name="description" />
        <meta {...attribs} />
      </>
    }
    if (name === 'meta' && attribs.name === 'robots') {
      return <meta {...attribs} content="index, follow" />
    }

    if (name === 'script' && attribs.class === 'yoast-schema-graph') {
      return <></>;
    }

    if (name === 'meta' && attribs.property === 'og:image') {
      const content = `https://res.cloudinary.com/dwqo3od7a/image/upload/ar_4:3,w_300,c_fill,f_auto/wp-remote/${attribs.content.replace(
        `${WP_CONTENT_API()}/uploads/`,
        ""
      )}`;
      return <meta {...attribs} content={content} />
    }
  }
}

export const itemLinkBasedOnCategory = (categoryName, slug) => {
  switch (categoryName) {
    case "Magazine": {
      return {
        href: "/magazine/[magazineslug]",
        as: `/magazine/${slug}`
      }
    }
    case "New Cars": {
      return {
        href: "/car/[[...params]]",
        as: `/car/${slug}`
      }
    }
    case "car": {
      return {
        href: "/car/[[...params]]",
        as: `/car/${slug}`
      }
    }
    case "motorcycle": {
      return {
        href: "/motorcycle/[[...params]]",
        as: `/motorcycle/${slug}`
      }
    }
    case 'company_profile': {
      return {
        href: "/company_profile/[[...params]]",
        as: `/company_profile/${slug}`
      }
    }
    case 'company_type': {
      return {
        href: "/company_type/[[...params]]",
        as: `/company_type/${slug}`
      }
    }
    case 'car_manufacturer': {
      return {
        href: "/car_manufacturer/[[...params]]",
        as: `/car_manufacturer/${slug}`
      }
    }
    case 'bike_manufacturer': {
      return {
        href: "/bike_manufacturer/[[...params]]",
        as: `/bike_manufacturer/${slug}`
      }
    }
    case 'category': {
      return {
        href: "/category/[[...params]]",
        as: `/category/${slug}`
      }
    }
    default: {
      return {
        href: "/[blogslug]",
        as: `/${slug}`
      }
    }
  }
}

export const hrefBasedOnObject = (object, url, slug) => {
  let newSlug = slug;
  if (!newSlug) {
    newSlug = url.replace(LEGACY_DOMAIN, '');
    newSlug = newSlug[0] === '/' ? newSlug.slice(1) : newSlug;
  }else{
    newSlug = newSlug.replace(LEGACY_DOMAIN, '');
    if (newSlug === "" || newSlug === "home-automark-magazine") {
      newSlug = "/";
    }
  }
  switch (object) {
    case 'category': {
      return '/category/[...params]'
    }
    case 'company_type': {
      return '/company_type/[[...params]]'
    }
    case 'company_profile': {
      return '/company_profile/[[...params]]'
    }
    case 'custom': {
      return newSlug
    }
    case 'page': {
      return newSlug
    }
  }
  return null;
}

export const asBasedOnObject = (object, url, slug) => {
  let newSlug = slug;
  if (!newSlug) {
    newSlug = url.replace(`${DOMAIN}/`, '').replace(LEGACY_DOMAIN, '');
    newSlug = newSlug[0] === '/' ? newSlug.slice(1) : newSlug;
  }
  switch (object) {
    case 'category': {
      return `/${object}/${newSlug}`
    }
    case 'company_type': {
      return `/${object}/${newSlug}`
    }
  }
  return null;
}

export const listViewForDoubleCategory = params => {
  // I am in pagination mode of an item
  if (params?.params?.length === 3) {
    return false;
  }


  if (params?.params?.length === 2) {
    // I am in pagination mode of self
    if (params.params.indexOf('page') >= 0) {
      return true;
    }
  }

  if (params?.params?.length === 1) {
    // I am on a slug of my category
    return false
  }

  // If there are no params, then I am on self root
  if (!params?.params?.length) {
    return true
  }
  return true;
}
export const getNavBar = async () => {
  const res = await fetch(`${DOMAIN}/wp-json/menus/v1/menus/182/`);
  return await res.json();
}

export const generateBreadCrumbDataForList = ({
  parentName,
  parentLink,
  categoryName
}) => {
  const json = [];
  json.push({
    "name": "Home",
    "item": "/"
  });

  if (parentLink) {
    json.push({
      "name": parentName,
      "item": parentLink.href,
      as: parentLink.as
    });
  }

  json.push({
    "name": categoryName
  });

  return json;
}
/*
[{
  "@type": "ListItem",
  "position": 1,
  "name": "Books",
  "item": "https://example.com/books"
},{
  "@type": "ListItem",
  "position": 2,
  "name": "Science Fiction",
  "item": "https://example.com/books/sciencefiction"
},{
  "@type": "ListItem",
  "position": 3,
  "name": "Award Winners"
}] 
*/
export const generateBreadCrumbDataForPost = ({ term, parentLink, parentName, title }) => {
  const json = [];
  json.push({
    "name": "Home",
    "item": "/"
  });

  if (parentLink && Array.isArray(parentLink) && parentLink.length > 0) {
    json.push(parentLink.map((pl, i) => ({
      "name": parentName[i],
      "item": pl.href,
      "as": pl.as
    })))
  }

  if (parentLink && !Array.isArray(parentLink)) {
    json.push({
      "name": parentName,
      "item": parentLink.href,
      "as": parentLink.as
    })
  }

  json.push({
    name: term.name,
    item: itemLinkBasedOnCategory(term.taxonomy, term.slug).href,
    as: itemLinkBasedOnCategory(term.taxonomy, term.slug).as
  })

  json.push({
    name: title
  })

  return json.flat();
}

/* {
  "@context": "https://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "name": "Books",
    "item": "https://example.com/books"
  },{
    "@type": "ListItem",
    "position": 2,
    "name": "Science Fiction",
    "item": "https://example.com/books/sciencefiction"
  },{
    "@type": "ListItem",
    "position": 3,
    "name": "Award Winners"
  }]
} */
export const getBreadCrumbsJson = (breadcrumbs) => {
  const json = {
    "@context": "https://schema.org",
    "@type": "BreadcrumbList",
    "itemListElement": []
  };

  json.itemListElement = breadcrumbs.map((breadcrumb, i) => {
    const item = {
      ...breadcrumb,
      '@type': 'ListItem',
      position: i + 1,
      ...(breadcrumb.item) && { item: `https://www.automark.pk${breadcrumb.as ? breadcrumb.as : breadcrumb.item}` },
    }
    delete item.as;
    return item;
  });

  return `<script type="application/ld+json">${JSON.stringify(json)}</script>`;
}

export const POST_API = () => {
  return `${DOMAIN}/wp-json/wp`;
}

export const WP_CONTENT_API = () => {
  return `${DOMAIN}/wp-content`;
}

export const LEGACY_WP_CONTENT_API = () => {
  return `${LEGACY_DOMAIN}/wp-content`;
}

export const HOST = 'admin.automark.pk';
export const DOMAIN = `https://${HOST}`;
export const LEGACY_DOMAIN = /(http(s)?:\/\/)?(www|admin)\.automark\.pk/g;

export const discourseKey = '441a28d58366f19d78d493dfa5545dbcaaae9296005015dce1f520291d058c07';