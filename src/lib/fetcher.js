const fetcher = (url) => {
  return new Promise((resolve, reject) => {
    fetch(url).then((raw) => {
      raw.json().then(data => {
        // console.log(data)
        resolve(data);
      }).catch(reject)
    }).catch(reject)
  })
}

export default fetcher;