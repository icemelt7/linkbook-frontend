export const createCompanyRemote = async (inputValue) => {
  return await (await fetch('/api/create-company', {
    method: 'post',
    body: JSON.stringify({
      name: inputValue
    })
  })).json();
}
