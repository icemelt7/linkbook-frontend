export default (state, action) => {
  switch (action.type) {
    case 'setUser': return {
      ...state, 
      ...action.payload,
      current: 'loaded'
    }
    case 'userDoesNotExit': return {
      ...state,
      current: 'notexist'
    }
    case 'setFormValue': return {
      ...state, 
      ...action.payload
    }
    case 'linkedin_loading_finish': return {
      ...state,
      linkedin_status: 'loading_finish'
    }
    case 'linkedin_loading_start': return {
      ...state,
      linkedin_status: 'loading_start'
    }
  }
}