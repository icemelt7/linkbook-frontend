import qs from "qs";
import { useReducer } from "react";

const reducer = (state, action) => {
  switch (action.type) {
    case "instantSearch":
      return { ...state, searchState: { ...action.payload } };
    case "pendingSearch":
      return { ...state, pendingSearchState: { ...action.payload } };
    case "pendingSearch":
      return {
        ...state,
        searchState: state.pendingSearchState,
        pendingSearchState: {}
      };
    case "forceClearRefine":
      return { ...state, searchState: {}, pendingSearchState: {} };
    default:
      throw new Error();
  }
};

export const useAlogliaHelpers = () => {
  const INSTANT_MODE = true;
  const [state, dispatch] = useReducer(reducer, {
    searchState: {}
  });

  const onSearchStateChange = searchState => {
    if (INSTANT_MODE) {
      dispatch({
        type: "instantSearch",
        payload: searchState
      });
    } else {
      dispatch({
        type: "pendingSearch",
        payload: searchState
      });
    }
  };

  const search = () => {
    dispatch({
      type: "commitPendingSearch"
    });
  };

  const forceClearRefine = () => {
    dispatch({
      type: "forceClearRefine"
    });
  };

  const createURL = state => `?${qs.stringify(state)}`;
  const searchStateToUrl = ({ location }, searchState) =>
    searchState ? `${location.pathname}${createURL(searchState)}` : "";

  const urlToSearchState = ({ search }) => qs.parse(search.slice(1));

  return {
    INSTANT_MODE,
    searchState: state.searchState,
    onSearchStateChange,
    search,
    forceClearRefine,
    createURL,
    searchStateToUrl,
    urlToSearchState
  };
};
