const level = "INFO"
const Logger = {
  log: (params) => {
    if (level === "INFO") {
      console.log(params);
    }
  }
}

export { Logger }