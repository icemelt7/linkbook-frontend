// Firebase App (the core Firebase SDK) is always required and must be listed first
const sanityClient = require('@sanity/client');
const client = sanityClient({
  projectId: '5gyrk3u0',
  dataset: 'production',
  useCdn: false // `false` if you want to ensure fresh data
});

import firebase from "firebase/app";

// Add the Firebase products that you want to use
import "firebase/auth";
import "firebase/storage";

import { listingThumbForProfilePic, userProfileFields } from "../constants";
import { Logger } from "./Logger";
import { string_to_slug } from "./helpers";
// TODO: Replace the following with your app's Firebase project configuration
const firebaseConfig = {
  apiKey: "AIzaSyBOUqERI5akC8v99JICsEdgmNZqPbewnm0",
  authDomain: "linkbook-f9f1d.firebaseapp.com",
  databaseURL: "https://linkbook-f9f1d.firebaseio.com",
  projectId: "linkbook-f9f1d",
  storageBucket: "linkbook-f9f1d.appspot.com",
  messagingSenderId: "626569944838"
};
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}
var storage = firebase.storage();
// Create a storage reference from our storage service
var storageRef = storage.ref();

export const isServer = () => {
  return (typeof window === "undefined");
}
// var DOMAIN = 'http://localhost:3000';
var actionCodeSettings =  {
  // URL you want to redirect back to. The domain (www.example.com) for this
  // URL must be whitelisted in the Firebase Console.
  url: (isServer() ? 'https://stage.automark.pk' : window.location.origin) + '/signincomplete',
  // This must be true.
  handleCodeInApp: true
};

const signInWithFirebase = ({ email }) => {
  return new Promise((resolve, reject) => {
    firebase.auth().sendSignInLinkToEmail(email, actionCodeSettings)
      .then(function () {
        // The link was successfully sent. Inform the user.
        // Save the email locally so you don't need to ask the user for it again
        // if they open the link on the same device.
        window.localStorage.setItem('emailForSignIn', email);
        resolve();
      })
      .catch(function (error) {
        // Some error occurred, you can inspect the code: error.code
        reject(error)
      });
  })

}

const verifyEmail = () => {
  return new Promise((resolve, reject) => {
    // Confirm the link is a sign-in with email link.
    if (firebase.auth().isSignInWithEmailLink(window.location.href)) {
      // Additional state parameters can also be passed via URL.
      // This can be used to continue the user's intended action before triggering
      // the sign-in operation.
      // Get the email if available. This should be available if the user completes
      // the flow on the same device where they started it.
      var email = window.localStorage.getItem('emailForSignIn');
      if (!email) {
        // User opened the link on a different device. To prevent session fixation
        // attacks, ask the user to provide the associated email again. For example:
        email = window.prompt('Please provide your email for confirmation');
      }
      // The client SDK will parse the code from the link for you.
      firebase.auth().signInWithEmailLink(email, window.location.href)
        .then(function (result) {
          // Clear email from storage.
          window.localStorage.removeItem('emailForSignIn');
          // You can access the new user via result.user
          // Additional user info profile not available via:
          // result.additionalUserInfo.profile == null
          // You can check if the user is new or existing:
          // result.additionalUserInfo.isNewUser
          resolve(result);
        })
        .catch(function (error) {
          // Some error occurred, you can inspect the code: error.code
          // Common errors could be invalid email and invalid or expired OTPs.
          reject(error)
        });
    }
  })
}

export const createUser = async (result) => {
  return await fetch('/api/create-user', {
    method: 'post',
    body: JSON.stringify({
      uid: result.user.uid,
      data: {
        email: result.user.email
      }
    })
  });
  // return await db.collection("users").doc(result.user.uid).set({
  //   email: result.user.email
  // }, { merge: true });
}

export const getUser = async (email) => {
  // Logger.log(uid)
  const query = `*[_type == "profile" && email == "${email}"]{
    ..., current_job_company_beta->
  }`;
  // Logger.log(query)
  const profiles = await client.fetch(query);
  // Logger.log(profiles)
  return profiles[0];
  // const _user = db.collection("users").doc(uid);

  // const doc = await _user.get();
  // if (doc.exists) {
  //   // Logger.log("Document data:", doc.data());
  //   return doc.data();
  // } else {
  //   // doc.data() will be undefined in this case
  //   Logger.log("No such document!");
  // }
}

export const updateUser = async ({ values }) => {
  // Logger.log(values);
  const uid = values.uid;
  // Extract Values
  const valuesToSave = {};
  userProfileFields.forEach(field => {
    if (values[field]) {
      valuesToSave[field] = values[field];
    }
  });
  // Logger.log(userProfileFields);
  // Logger.log(valuesToSave)
  // Add additional Items
  if (values.fullName) {
    valuesToSave.slug = string_to_slug(values.fullName, '-');
  }
  
  var user = firebase.auth().currentUser;
  user.updateProfile({
    displayName: values.fullName ? values.fullName : '',
    email: values.email ? values.email : '',
    phoneNumber: values.phone_number ? values.phone_number : '',
    photoURL: values.picture ? listingThumbForProfilePic(values.picture) : ''
  }).then(function () {
    // Update successful.
  }).catch(function (error) {
    Logger.log(error);
  });
  console.log(valuesToSave);
  return await fetch('/api/create-user', {
    method: 'post',
    body: JSON.stringify({
      uid,
      data: {
        ...valuesToSave
      }
    })
  });
}

if (typeof window !== 'undefined') {
  window._firebase = firebase;
}

const S = client;
export { firebase, signInWithFirebase, verifyEmail, storageRef, S };