
function decodeHTML(str){
  return str.replace(/&#([0-9]{1,3});/gi, function(match, num) {
      return String.fromCharCode(parseInt(num));
  });
}
const cleanSummary = (summary) => {
  return decodeHTML(summary.split('</p>')[0].replace('<p>', '').replace('[&hellip;]', ''));
}

export default cleanSummary;