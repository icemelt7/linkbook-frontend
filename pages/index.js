import Head from 'next/head';
import parse from 'html-react-parser';
import FooterBlog from "../src/components/FooterBlog";
import FeaturedBlog from "../src/components/Blog/FeaturedBlog";
import NavBar from "../src/components/Blog/NavBar";
import { POST_API, seoParseOptions, getNavBar } from "../src/lib/helpers";
import { PostList } from '../src/components/Blog/PostsList';
import SearchBar from '../src/components/Blog/SearchBar';
import { Title } from '../src/components/Blog/Label';
import Link from "next/link";

const Index = ({ posts, homeAPI, navBar }) => {
  const { yoast_head } = homeAPI;

  const seoTitle = yoast_head.match(/<meta property=\"og:title\" content=\"(.*)\" \/>/)

  const featuredPosts = posts.filter(post => post.jetpack_featured_media_url).slice(0, 4);
  return (
    <>
      <Head>
        {parse(yoast_head, seoParseOptions)}
        {parse(`<script type="application/ld+json">
        {
          "@context": "https://schema.org",
          "@type": "WebSite",
          "url": "https://www.automark.pk/",
          "potentialAction": {
            "@type": "SearchAction",
            "target": "https://www.automark.pk/search?q={search_term_string}",
            "query-input": "required name=search_term_string"
          }
        }
        </script>`)}
        <title>{parse(seoTitle[1])}</title>
      </Head>
      <NavBar navBar={navBar} />
      <div className="max-w-6xl mx-auto md:px-16 px-7">
        <div className="md:max-w-none max-w-xs block mt-10 md:mx-0 mx-auto mb-4">
          <Title>Whats happening in the Automobile Industry</Title>
        </div>
        <SearchBar />
        <FeaturedItems
          featuredItems={featuredPosts}
        />
        <div className="md:flex md:justify-between md:flex-wrap">
          <PostList posts={posts} />

        </div>
        <Link href="/category/[...params]" as="/category/automotive-articles" prefetch={false}>
          <a className="text-2xl font-bold text-blue-600 underline">
            Click here to see all Automotive Articles
          </a>
        </Link>
      </div>
      <FooterBlog />
    </>
  );
};

const FeaturedItems = ({ featuredItems }) => {
  const [p1, p2, p3, p4] = featuredItems;

  return (
    <div className="sm:grid sm:grid-cols-12 sm:gap-7 mb-12 bp:mb-13">
      <div className="sm:col-span-6 sm:mb-0 mb-4 min-w-full">
        <FeaturedBlog post={p1} />
      </div>
      <div className="sm:col-span-6 sm:mb-0 mb-4 min-w-full">
        <FeaturedBlog post={p2} />
      </div>
      <div className="sm:col-span-6 sm:mb-0 mb-4 min-w-full">
        <FeaturedBlog post={p3} />
      </div>
      <div className="sm:col-span-6 sm:mb-0 mb-4 min-w-full">
        <FeaturedBlog post={p4} />
      </div>
    </div>
  );
};


export async function getStaticProps() {
  let postsRaw = await fetch(`${POST_API()}/v2/posts?_embed=wp:term`);
  const headers = postsRaw.headers;
  let posts = await postsRaw.json();
  let homeAPI = await (await fetch(`${POST_API()}/v2/pages?slug=home-automark-magazine`)).json();
  homeAPI = homeAPI[0];

  const navBar = await getNavBar();
  // Return the results
  return {
    props: {
      posts,
      homeAPI,
      navBar,
    },
  };
}

export default Index;
