import Head from 'next/head';
import { useState, useEffect } from 'react';
import algoliasearch from 'algoliasearch/lite';
import { PoweredBy, Configure, Pagination, InstantSearch, SearchBox, Hits } from 'react-instantsearch-dom';
import FooterBlog from "../src/components/FooterBlog";
import NavBar from "../src/components/Blog/NavBar";
import { AlgoliaNormalBlog } from '../src/components/NormalBlogs';
import { getNavBar } from '../src/lib/helpers';

const searchClient = algoliasearch('50Y3FF3FRR', '74445ca82c34f34759bce7f23908fb87');

const Index = ({ navBar }) => {
  const [searchState, setSearchState] = useState({});
  useEffect(() => {
    const search = new URL(window.location);
    const query = search.searchParams.get('q');
    setSearchState({ query });
  }, []);
  return (
    <>
      <Head>
        <title>Search across the #1 Automotive news source - Automark.pk</title>
      </Head>
      <NavBar navBar={navBar} />
      <div className="max-w-6xl mx-auto md:px-16 px-7">
        <div className="md:max-w-none max-w-xs block mt-10 md:mx-0 mx-auto mb-4">
          <Title>Search across the #1 Automotive news source - Automark.pk</Title>
        </div>
        
        <div className="md:flex md:justify-between md:flex-wrap md:flex-col">
            <InstantSearch 
              searchClient={searchClient} 
              searchState={searchState}
              onSearchStateChange={setSearchState}
              indexName="wp_searchable_posts"
            >
                <Configure 
                    hitsPerPage={4} 
                    attributesToRetrieve={[
                        '*',
                        '-content',
                    ]}
                    attributesToSnippet={null}
                    attributesToHighlight={[
                        'post_title'
                    ]}
                />
                <div className="flex">
                    <SearchBox />
                </div>
                <Hits hitComponent={AlgoliaNormalBlog} />
                <Pagination />
                <PoweredBy />
            </InstantSearch>
            <style global jsx>{`
                .ais-SearchBox {
                    margin-bottom: 32px;
                }
                .ais-PoweredBy {
                    margin-left: auto;
                }
                .ais-SearchBox-input {
                    padding: 8px;
                    border: 1px solid black;
                    border-radius: 4px;
                    width: 320px;
                }
                .ais-SearchBox-input:focus {
                    border: none;
                }

                .ais-SearchBox-resetIcon,
                .ais-SearchBox-submit {
                    display: none;
                }
                .ais-Pagination-list {
                    display: flex
                }
                .ais-Pagination-link {
                    color: #1c64f2;
                    text-decoration: underline;
                }
                .ais-Pagination-item {
                    padding: 12px;
                }
            `}</style>
        </div>
      </div>
      <FooterBlog />
    </>
  );
};
const Title = ({ children }) => {
  return (
    <>
      <p className="text-3xl font-bold leading-8 text-cool-gray-900 mb-3">
        {children}
      </p>
      <div className="border-b border-orange-500 w-8 h-1 mb-2"></div>
    </>
  );
};

export async function getStaticProps({ }) {
  const navBar = await getNavBar();
  // Return the results
  return {
    props: {
      navBar,
    },
  };
}

export default Index;
