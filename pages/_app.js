// import LogRocket from 'logrocket';
// import setupLogRocketReact from 'logrocket-react';
import Head from 'next/head';
import { useEffect } from 'react'
import Router from 'next/router'
import * as gtag from '../src/lib/gtag'
// import NProgress from 'nprogress';
// import 'nprogress/nprogress.css';
import "./admin.css";

// Router.events.on('routeChangeStart', url => {
//   NProgress.start()
// })
// Router.events.on('routeChangeComplete', () => NProgress.done())
// Router.events.on('routeChangeError', () => NProgress.done())
// <div dangerouslySetInnerHTML={{__html: `
//         <!-- Global site tag (gtag.js) - Google Analytics -->
//         <script async src="https://www.googletagmanager.com/gtag/js?id=UA-17971208-1"></script>
//         <script>
//           window.dataLayer = window.dataLayer || [];
//           function gtag(){dataLayer.push(arguments);}
//           gtag('js', new Date());

//           gtag('config', 'UA-17971208-1');
//         </script>

//       `}}/>
const App = ({ Component, pageProps }) => {
  useEffect(() => {
    const handleRouteChange = (url) => {
      gtag.pageview(url)
      FB.AppEvents.logPageView();
    }
    Router.events.on('routeChangeComplete', handleRouteChange)
    return () => {
      Router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [])

  return <>
    <Head>
      <meta charSet="utf-8" />
      {/* Use minimum-scale=1 to enable GPU rasterization */}
      <meta
        name="viewport"
        content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no"
      />
      <link rel="shortcut icon" href="/static/favicon.png" />
      <meta name="google-site-verification" content="WXNcX2mQYccRDRRr4qVzlRhxp-iZ-jnLtYU_oVJ8wOg" />
      <meta property="fb:app_id" content="423697211552174"/>
      <link href="https://res.cloudinary.com" rel="preconnect" crossOrigin="true" />
      <link href="https://fonts.gstatic.com" rel="preconnect" crossOrigin="true" />
      <link rel="preload" as="style" href="https://fonts.googleapis.com/css2?family=PT+Serif&display=swap" />
      <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=PT+Serif&display=swap" media="print" onLoad={() => this.media='all' } />
      <script
        async
        src={`https://www.googletagmanager.com/gtag/js?id=${gtag.GA_TRACKING_ID}`}
      />
      <script
        dangerouslySetInnerHTML={{
          __html: `
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', '${gtag.GA_TRACKING_ID}', {
            page_path: window.location.pathname,
          });
        `,
        }}
      />
    </Head>
    <Component {...pageProps} />
    <script dangerouslySetInnerHTML={{
      __html: `
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '423697211552174',
          xfbml      : true,
          version    : 'v8.0'
        });
        FB.AppEvents.logPageView();
      };

      (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));` }}
    />
  </>
}

export default App