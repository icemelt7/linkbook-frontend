
import { POST_API, getNavBar } from "../src/lib/helpers";
import Post from "../src/components/Blog/Post";

const BlogSlug = ({ post, navBar, parentName, parentLink }) => {
  return <Post post={post} navBar={navBar} parentName={parentName} parentLink={parentLink} />
};

export async function getStaticPaths() {
  return {
    paths: [{
      params: {
        blogslug: 'a-new-suv-competitor-in-the-market-hyundai-tucson'
      },
    }],
    fallback: true
  }
}
export async function getStaticProps({ params }) {
  const { blogslug } = params;
  if (blogslug.includes(".")) {
    return { props: {} };
  }

  let post = await (await fetch(`${POST_API()}/v2/posts?slug=${blogslug}&_embed=author,wp:term`)).json();
  post = post[0];

  const navBar = await getNavBar();
  // Return the results
  return {
    props: {
      post,
      navBar,
      parentName: 'All Categories',
      parentLink: { as: '/category', href: '/category/[[...params]]' },
    },
  };
}

export default BlogSlug;
