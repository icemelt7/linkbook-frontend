import { POST_API, getNavBar, listViewForDoubleCategory } from '../../src/lib/helpers';
import Post from '../../src/components/Blog/Post';
import List from '../../src/components/Blog/List';
import { useRouter } from 'next/router';

const CarSlugOrList = ({
  totalPages,
  listView,
  post,
  posts,
  categoryName,
  navBar,
  currentPage,
  parentLink,
  parentName,
  preferType,
}) => {
  const router = useRouter();
  if (router.isFallback) {
    return <div>Loading...</div>;
  }
  if (!listView) {
    if (!post) {
      return <></>;
    }
    const { acf } = post;
    return (
      <Post 
        post={post} 
        navBar={navBar} 
        parentLink={parentLink} 
        parentName={parentName}
      >
        {acf.price && <h2>Starting from {new Intl.NumberFormat('en-IN').format(acf.price)} PKR</h2>}
        {acf.horse_power && <h3>{acf.horse_power}cc</h3>}
      </Post>
    );
  }
  return (
    <List
      rootslug="car"
      preferType={preferType}
      posts={posts}
      categoryName={categoryName}
      currentPage={currentPage}
      display_type="list"
      totalPages={totalPages}
      navBar={navBar}
    />
  );
};

export async function getStaticPaths() {
  // let posts = await (await fetch(`${POST_API()}/v2/car`)).json();
  return {
    paths: [
      {
        params: {
          params: [],
        },
      },
    ],
    fallback: true,
  };
}

export async function getStaticProps({ params }) {
  const navBar = await getNavBar();
  if (listViewForDoubleCategory(params)) {
    const catUrl = new URL(`${POST_API()}/v2/car`);
    catUrl.searchParams.set('_embed', 'wp:term');
    let page = null;
    if (params?.params?.length > 1) {
      page = params.params[1];
      catUrl.searchParams.set('page', page);
    }

    const catDataRaw = await fetch(catUrl.toString());
    const catDataJson = await catDataRaw.json();

    return {
      props: {
        posts: catDataJson,
        categoryName: 'New Cars',
        currentPage: page || 1,
        totalPages: catDataRaw.headers.get('x-wp-totalpages'),
        navBar,
        listView: true,
        preferType: true
      },
    };
  }

  // Slug View
  const [carSlug] = params?.params;
  let post = await (
    await fetch(`${POST_API()}/v2/car?slug=${carSlug}&_embed=author,wp:term`)
  ).json();
  post = post[0];

  return {
    props: {
      post,
      navBar,
      parentName: ['All Manufacturers', 'Car Manufacturers', 'All Cars'],
      parentLink: [
        {
          href: '/company_type/[[...params]]',
          as: '/company_type',
        },
        {
          href: '/car_manufacturer/[[...params]]',
          as: '/car_manufacturer',
        },
        {
          href: '/car',
          as: '/car',
        },
      ],
    },
  };
}

export default CarSlugOrList;
