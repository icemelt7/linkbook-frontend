import { useEffect } from "react"

export default () => {
    useEffect(() => {
        window.location.href = `https://admin.automark.pk/${window.location.pathname}`;
    }, [])
    return <div>404</div>
}