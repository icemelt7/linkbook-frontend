import Head from 'next/head';
import Image from 'next/image'
import FooterBlog from '../src/components/FooterBlog';
import NavBar from '../src/components/Blog/NavBar';
import { getNavBar } from '../src/lib/helpers';
import { useState } from 'react';

const contactUs = ({ navBar }) => {
  const [message, setMessage] = useState({
    Status: true,
    Message: '',
  });
  const handleSubmit = async e => {
    e.preventDefault();
    const form = e.target;
    try {
      const params = new URLSearchParams([...new FormData(form).entries()]);
      const message = [...new FormData(form).entries()].map((entry) => {
        return `${entry[0]}: ${entry[1]} - `;
      }).join(' - ');

      await fetch('https://admin.automark.pk/automark_mail.php', {
        method: 'POST',
        mode: 'no-cors',
        body: params,
      });
      form.reset();
      window.open(`https://api.whatsapp.com/send?phone=923212203815&text=${message}`,'_blank');
      setMessage({
        Status: true,
        Message: 'Thank you for submitting. We will contact you shortly',
      });
    } catch (error) {
      console.log(error);
      setMessage({ Status: false, message: 'The form could not be processed. Try again' });
    }
  };
  return (
    <div>
      <Head>
        <title>Automark - Contact Us</title>
      </Head>
      <NavBar navBar={navBar} />
      <div className="max-w-6xl mx-auto  md:px-16 px-7">
        <div className="mt-10 flex flex-col md:grid md:grid-cols-2 md:h-96">
          <div className="bg-gray-100 p-8 flex flex-col justify-center md:h-96 h-64">
            <p className="text-3xl font-bold text-cool-gray-900 mb-3 tracking-tight">
              Just drop us your details We'll contact you with details on our magazine subscription
              plans
            </p>

            <div className="flex items-center">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="fill-current text-gray-500 h-4 w-4 mr-4"
                viewBox="0 0 20 20"
              >
                <path d="M10 20S3 10.87 3 7a7 7 0 1 1 14 0c0 3.87-7 13-7 13zm0-11a2 2 0 1 0 0-4 2 2 0 0 0 0 4z" />
              </svg>
              <p className="font-medium text-gray-500 text-base mb-2">
                {' '}
                D-68 Block 9 Clifton Karachi-77560, Pakistan
              </p>
            </div>
            <div className="flex items-center">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="fill-current text-gray-500 h-4 w-4 mr-4"
                viewBox="0 0 20 20"
              >
                <path d="M20 18.35V19a1 1 0 0 1-1 1h-2A17 17 0 0 1 0 3V1a1 1 0 0 1 1-1h4a1 1 0 0 1 1 1v4c0 .56-.31 1.31-.7 1.7L3.16 8.84c1.52 3.6 4.4 6.48 8 8l2.12-2.12c.4-.4 1.15-.71 1.7-.71H19a1 1 0 0 1 .99 1v3.35z" />
              </svg>
              <p className="font-medium text-gray-500 text-base mb-2">+92 321 2203815</p>
            </div>
            <div className="flex items-center">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="fill-current text-gray-500 h-4 w-4 mr-4"
                viewBox="0 0 20 20"
              >
                <path d="M18 2a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4c0-1.1.9-2 2-2h16zm-4.37 9.1L20 16v-2l-5.12-3.9L20 6V4l-10 8L0 4v2l5.12 4.1L0 14v2l6.37-4.9L10 14l3.63-2.9z" />
              </svg>
              <p className="font-medium text-gray-500 text-base">
                <a href="mailto: automarkpk@gmail.com">automarkpk@gmail.com</a>
              </p>
            </div>
          </div>
          <form
            onSubmit={handleSubmit}
            className="md:px-8 md:py-0 pt-8 flex flex-col h-96 justify-between"
          >
            <div>
              <input
                type="text"
                name="name"
                id="full_name"
                autoComplete="name"
                placeholder="Full name"
                className="mt-1 focus:ring-gray-500 focus:border-gray-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
              />
            </div>

            <div>
              <input
                type="text"
                name="email"
                id="email_address"
                autoComplete="email"
                placeholder="Email"
                className="mt-1 focus:ring-gray-500 focus:border-gray-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
              />
            </div>

            <div>
              <input
                type="text"
                name="phone"
                id="phone"
                autoComplete="tel"
                placeholder="Phone Number"
                className="mt-1 focus:ring-gray-500 focus:border-gray-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
              />
            </div>

            <div>
              <textarea
                type="text"
                name="msg"
                id="message"
                placeholder="Your Message"
                className="mt-1 focus:ring-gray-500 focus:border-gray-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
              />
            </div>

            <div className="md:text-left text-center">
              <button
                type="submit"
                className="inline-flex justify-center items-center py-2 px-4 border shadow-sm text-sm font-medium rounded-md text-red focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500"
              >
                Send Message <div className="flex ml-1"><Image src="/static/images/whatsapp.png" alt="whatsapp" width={25} height={25} /></div>
              </button>
            </div>
          </form>
        </div>
      </div>
      <div className="mt-4 px-8">
        {message.Status ? (
          <p className="text-lg text-center text-green-700 font-bold">{message.Message}</p>
        ) : (
          <p className="text-lg text-center text-red-700 font-bold">{message.Message}</p>
        )}
      </div>
      <FooterBlog />
    </div>
  );
};
export async function getStaticProps() {
  const navBar = await getNavBar();
  // Return the results
  return {
    props: {
      navBar,
    },
  };
}
export default contactUs;
