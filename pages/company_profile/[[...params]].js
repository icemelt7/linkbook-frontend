import { POST_API, getNavBar } from "../../src/lib/helpers";
import Post from "../../src/components/Blog/Post";
import { useRouter } from "next/router";
import List from "../../src/components/Blog/List";

const CompanyProfile = ({
  totalPages,
  listView,
  post,
  posts,
  categoryName,
  navBar,
  currentPage,
  parentLink,
  parentName
}) => {
  const router = useRouter()
  if (router.isFallback) {
    return <div>Loading...</div>
  }
  if (!listView) {
    if (!post) {
      return <></>;
    }
    return <Post post={post} parentLink={parentLink} parentName={parentName} navBar={navBar} />
  }
  return (
    <List
      rootslug="company_profile"
      posts={posts}
      categoryName={categoryName}
      currentPage={currentPage}
      display_type="list"
      totalPages={totalPages}
      parentLink={parentLink} 
      parentName={parentName}
      navBar={navBar}
    />
  );
};

export async function getStaticPaths() {
  return {
    paths: [
      {
        params: {
          params: [],
        },
      },
    ],
    fallback: true,
  };
}

const listView = params => {
  if (params?.params?.length > 0) {
    if (params.params.indexOf('page') >= 0) {
      return true;
    } else {
      return false;
    }
  } else {
    return true;
  }
};

export async function getStaticProps({ params }) {
  const navBar = await getNavBar();
  if (listView(params)) {
    const catUrl = new URL(`${POST_API()}/v2/company_profile`);
    catUrl.searchParams.set('_embed', 'wp:term');
    let page = null;
    if (params?.params?.length > 1) {
      page = params.params[1];
      catUrl.searchParams.set('page', page);
    }

    const catDataRaw = await fetch(catUrl.toString());
    const catDataJson = await catDataRaw.json();

    return {
      props: {
        posts: catDataJson,
        categoryName: 'Company Profiles',
        currentPage: page || 1,
        totalPages: catDataRaw.headers.get('x-wp-totalpages'),
        navBar,
        listView: true,
      },
    };
  }

  // Slug View
  const [companySlug] = params?.params;
  const postUrl = new URL(`${POST_API()}/v2/company_profile`);
  postUrl.searchParams.set('_embed', 'author, wp:term');
  postUrl.searchParams.set('slug', companySlug);
  let post = await (await fetch(postUrl.toString())).json();
  post = post[0];
  // const html = parse(post.content.rendered);
  return {
    props: {
      parentName: 'All Manufacturers',
      parentLink: {as: '/company_type', href: '/company_type/[[...params]]'},
      post,
      navBar,
    },
  };
}
export default CompanyProfile;
