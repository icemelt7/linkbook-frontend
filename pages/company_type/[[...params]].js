import { getNavBar, DOMAIN, POST_API } from '../../src/lib/helpers';
import Post from '../../src/components/Blog/Post';
import List from '../../src/components/Blog/List';
import { useRouter } from 'next/router';

const CarSlugOrList = ({
  totalPages,
  listView,
  post,
  posts,
  categoryName,
  navBar,
  currentPage,
  rootslug,
  parentName,
  parentLink
}) => {
  const router = useRouter()
  if (router.isFallback) {
    return <div>Loading...</div>
  }
  if (!listView) {
    if (!post) {
      return <div>Loading...</div>;
    }
    return <Post post={post} navBar={navBar} />;
  }

  return (
    <List
      rootslug={rootslug}
      posts={posts}
      categoryName={categoryName}
      currentPage={currentPage}
      display_type="list"
      totalPages={totalPages}
      navBar={navBar}
      parentName={parentName}
      parentLink={parentLink}
    />
  );
};

const listView = params => {
  if (params?.params?.length > 0) {
    if (params.params.indexOf('page') >= 0) {
      return true;
    } else {
      return false;
    }
  } else {
    return true;
  }
};

export async function getStaticProps({ params }) {
  try {
    const navBar = await getNavBar();
    if (listView(params)) {
      const catUrl = new URL(`${POST_API()}/v2/company_type`);
      catUrl.searchParams.set('_embed', 'wp:term, wp:post_type');
      let page = null;
      if (params?.params?.length > 1) {
        page = params.params[1];
        catUrl.searchParams.set('page', page);
      }

      const catDataRaw = await fetch(catUrl.toString());
      const catDataJson = await catDataRaw.json();
      return {
        props: {
          rootslug: 'company_type',
          posts: catDataJson,
          categoryName: 'Manufacturers',
          currentPage: page || 1,
          totalPages: catDataRaw.headers.get('x-wp-totalpages'),
          navBar,
          listView: true,
        },
      };
    }
    const slug = params.params[0];
    const catUrl = new URL(`${DOMAIN}/wp-json/wp/v2/company_type`);
    catUrl.searchParams.set('slug', slug);
    let category = (await (await fetch(catUrl)).json())[0];

    const { id, acf } = category;

    let page = null;
    if (params.params.length > 2) {
      page = params.params[2];
    }
    const url = new URL(`${DOMAIN}/wp-json/wp/v2/company_profile`);
    url.searchParams.set('company_type', id);
    url.searchParams.set('_embed', 'wp:term');

    if (page) {
      url.searchParams.set('page', page);
    }

    if (acf?.display_type === 'Grid') {
      url.searchParams.set('per_page', 9);
    }
    const catDataRaw = await fetch(url.toString());
    const catDataJson = await catDataRaw.json();
    
    return {
      props: {
        ...acf?.display_type && { display_type: acf?.display_type },
        posts: catDataJson,
        categoryName: `${catDataJson?.[0]?.['_embedded']?.['wp:term']?.[0]?.[0]?.name}s`,
        currentPage: page || 1,
        totalPages: catDataRaw.headers.get('x-wp-totalpages'),
        navBar,
        rootslug: 'company_profile',
        parentName: 'All Manufacturers',
        parentLink: {as: '/company_type', href: '/company_type/[[...params]]'},
        listView: true,
      },
    }
  } catch (e) {
    console.log(e)
  }
}
export async function getStaticPaths() {
  return {
    paths: [
      {
        params: {
          params: []
        }
      }
    ],
    fallback: true
  };
}

export default CarSlugOrList;
