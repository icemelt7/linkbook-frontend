import { DOMAIN, getNavBar, POST_API, listViewForDoubleCategory } from '../../src/lib/helpers';
import List from '../../src/components/Blog/List';

// we need to make this page bro
const CategoryPage = ({ 
  error, 
  posts, 
  categoryName, 
  currentPage, 
  display_type, 
  totalPages, 
  navBar, 
  rootslug, 
  parentName, 
  parentLink 
}) => {
  if (error) {
    return <div>{error}</div>
  }
  return (
    <List
      posts={posts}
      categoryName={categoryName}
      currentPage={currentPage}
      display_type={display_type}
      totalPages={totalPages}
      navBar={navBar}
      rootslug={rootslug}
      parentName={parentName}
      parentLink={parentLink}
    />
  );
};

export async function getStaticProps({ params }) {
  try {
    console.log(params)
    const navBar = await getNavBar();
    if (listViewForDoubleCategory(params)) {
      const catUrl = new URL(`${POST_API()}/v2/categories`);
      catUrl.searchParams.set('_embed', 'wp:term');
      let page = null;
      if (params?.params?.length > 1) {
        page = params.params[1];
        catUrl.searchParams.set('page', page);
      }

      const catDataRaw = await fetch(catUrl.toString());
      const catDataJson = await catDataRaw.json();
      return {
        props: {
          rootslug: 'category',
          posts: catDataJson,
          categoryName: 'Categories',
          currentPage: page || 1,
          totalPages: catDataRaw.headers.get('x-wp-totalpages'),
          navBar,
          listView: true,
        },
      };
    }

    // Single category view
    const slug = params.params[0];
    const catUrl = new URL(`${DOMAIN}/wp-json/wp/v2/categories`);
    catUrl.searchParams.set('slug', slug);
    catUrl.searchParams.set('_embed', 'wp:term');
    
    let category = null;
    try {
      let categoryRaw = await fetch(catUrl);
      category = await categoryRaw.json();
      category = category[0];
    }catch(e){
      console.log(e);
      return {
        props: {
          error: JSON.stringify(e)
        }
      }
    }

    const { id, acf } = category;

    let page = null;
    if (params.params.length > 2) {
      page = params.params[2];
    }
    const url = new URL(`${DOMAIN}/wp-json/wp/v2/posts`);
    url.searchParams.set('categories', id);
    url.searchParams.set('_embed', 'wp:term');

    if (page) {
      url.searchParams.set('page', page);
    }

    if (acf?.display_type === 'Grid') {
      url.searchParams.set('per_page', 9);
    }
    const catDataRaw = await fetch(url.toString());
    const catDataJson = await catDataRaw.json();

    return {
      props: {
        ...(acf?.display_type && { display_type: acf?.display_type }),
        posts: catDataJson,
        categoryName: catDataJson?.[0]?.['_embedded']?.['wp:term']?.[0]?.[0]?.name,
        currentPage: page || 1,
        totalPages: catDataRaw.headers.get('x-wp-totalpages'),
        navBar,
        parentName: 'All Categories',
        parentLink: { as: '/category', href: '/category/[[...params]]' },
        listView: true,
      },
    };
  } catch (e) {
    console.log(e);
  }
}
export async function getStaticPaths() {
  return {
    paths: [
      {
        params: {
          params: [],
        },
      },
    ],
    fallback: true,
  };
}

export default CategoryPage;
