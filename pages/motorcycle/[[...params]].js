import { POST_API, getNavBar } from '../../src/lib/helpers';
import Post from '../../src/components/Blog/Post';
import List from '../../src/components/Blog/List';
import { useRouter } from 'next/router';

const BikeSlugOrList = ({
  totalPages,
  listView,
  post,
  posts,
  categoryName,
  navBar,
  currentPage,
  parentLink,
  parentName,
  preferType
}) => {
  const router = useRouter();
  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  if (!listView) {
    if (!post) {
      return <div>Loading...</div>;
    }
    const { acf } = post;
    return (
      <Post 
        parentName={parentName}
        parentLink={parentLink}
        post={post} 
        navBar={navBar}
      >
        {acf.price && <h2>Starting from {new Intl.NumberFormat('en-IN').format(acf.price)} PKR</h2>}
        {acf.horse_power && <h3>{acf.horse_power}cc</h3>}
      </Post>
    );
  }
  return (
    <List
      rootslug="motorcycle"
      posts={posts}
      categoryName={categoryName}
      currentPage={currentPage}
      display_type="Grid"
      totalPages={totalPages}
      navBar={navBar}
      parentName={parentName}
      parentLink={parentLink}
      preferType={preferType}
    />
  );
};

export async function getStaticPaths() {
  // let posts = await (await fetch(`${POST_API()}/v2/car`)).json();
  return {
    paths: [
      {
        params: {
          params: [],
        },
      },
    ],
    fallback: true,
  };
}

const listView = params => {
  if (params?.params?.length > 0) {
    if (params.params.indexOf('page') >= 0) {
      return true;
    } else {
      return false;
    }
  } else {
    return true;
  }
};

export async function getStaticProps({ params }) {
  const navBar = await getNavBar();
  if (listView(params)) {
    const catUrl = new URL(`${POST_API()}/v2/motorcycle`);
    catUrl.searchParams.set('_embed', 'wp:term');

    catUrl.searchParams.set('per_page', 9);
    let page = null;
    if (params?.params?.length > 1) {
      page = params.params[1];
      catUrl.searchParams.set('page', page);
    }

    const catDataRaw = await fetch(catUrl.toString());
    const catDataJson = await catDataRaw.json();

    return {
      props: {
        posts: catDataJson,
        categoryName: 'New Motorcycles',
        currentPage: page || 1,
        totalPages: catDataRaw.headers.get('x-wp-totalpages'),
        navBar,
        listView: true,
        preferType: true
      },
    };
  }

  // Slug View
  const [carSlug] = params?.params;
  let post = await (
    await fetch(`${POST_API()}/v2/motorcycle?slug=${carSlug}&_embed=author,wp:term`)
  ).json();
  post = post[0];
  return {
    props: {
      post,
      navBar,
      parentName: ['All Manufacturers', 'Motorcycle Manufacturers', 'All Motorcycles'],
      parentLink: [
        {
          href: '/company_type/[[...params]]',
          as: '/company_type',
        },
        {
          href: '/bike_manufacturer/[[...params]]',
          as: '/bike_manufacturer',
        },
        {
          href: '/motorcycle',
          as: '/motorcycle',
        },
      ],
    },
  };
}

export default BikeSlugOrList;
