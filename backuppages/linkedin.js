import React from 'react';
import LinkedInPopUp from '../src/lib/LinkedInPopup'

const Linkedin = props => {
  return <LinkedInPopUp {...props} />
};

export default Linkedin;