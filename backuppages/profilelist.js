import React from "react";
import NProgress from "nprogress";
import algoliasearch from "algoliasearch/lite";
import { findResultsState } from "react-instantsearch-dom/server";
import { InstantSearch } from 'react-instantsearch-dom';
import NavTail from "../src/components/NavTail";
import Filter from "../src/components/Filter";
import Hits from '../src/components/ProfileList/Hits';
import Pagination from '../src/components/ProfileList/Pagination';
import { useAlogliaHelpers } from "../src/lib/algoliaHooks";
import CustomStateResults from "../src/components/ProfileList/SearchState";

const searchClient = algoliasearch(
  "AFXL19G06C",
  "eb1b8137dfd7576b3e3e2269cfa30dd5"
);

const ProfileList = ({ profiles, resultsState }) => {
  const {
    INSTANT_MODE,
    searchState,
    onSearchStateChange,
    search,
    forceClearRefine,
    createURL,
  } = useAlogliaHelpers();


  return (
    <div className="bg-gray-200 min-h-screen">
      <NavTail />
      <div className="sm:flex block wrapper">
        <InstantSearch
          searchClient={searchClient}
          resultsState={resultsState}
          searchState={searchState}
          onSearchStateChange={onSearchStateChange}
          createURL={createURL}
          indexName="profiles"
        >
          <div className="sideBar sm:block hidden w-64">
            <Filter
              INSTANT_MODE={INSTANT_MODE}
              forceClearRefine={forceClearRefine}
            />
            {!INSTANT_MODE && <button onClick={search}>Search</button>}
          </div>

          <div className="topBar sm:hidden block">
            <Filter
              INSTANT_MODE={INSTANT_MODE}
              forceClearRefine={forceClearRefine}
            />
            {!INSTANT_MODE && <button onClick={search}>Search</button>}
          </div>
          <div className="sm:w-1/2 w-full main mt-4">
            <CustomStateResults />
            <Hits />
            <Pagination />
          </div>
        </InstantSearch>
        </div>
    </div>
  );
};

ProfileList.getInitialProps = async ({ res, req, query }) => {
  if (res) {
    res.setHeader("Cache-Control", `s-maxage=5, stale-while-revalidate`);
  }
  if (!req) {
    NProgress.done();
    return {};
  } else {
    const resultsState = await findResultsState(ProfileList, {
      searchClient,
      indexName: 'profiles',
    });
    return { resultsState };
  }
};

export default ProfileList;
