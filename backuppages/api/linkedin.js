import { Logger } from "../../src/lib/Logger";

const querystring = require("querystring");
const axios = require("axios");
var cloudinary = require("cloudinary");
cloudinary.config({
  cloud_name: "dwqo3od7a",
  api_key: "774871895111732",
  api_secret: "LMENkgzcZ5BjxU85HK0OUk_aLLk"
});

const client_id = "81fd49jjbco0tf";
const client_secret = "BP3qi9G8AqJhq51W";

//import { db } from '../../src/lib/firebase';
export default (req, res) => {
  const redirect_uri =
    (req.headers.host.includes("localhost") ? "http://" : "https://") +
    req.headers.host +
    "/linkedin";
  Logger.log(redirect_uri);
  const code = JSON.parse(req.body).code;
  const userId = JSON.parse(req.body).userId;

  axios
    .post(
      "https://www.linkedin.com/oauth/v2/accessToken",
      querystring.stringify({
        grant_type: "authorization_code",
        code,
        redirect_uri,
        client_id,
        client_secret
      })
    )
    .then(a => {
      console.log(a.data.access_token);

      var config = {};

      console.log(config);
      axios({
        method: "get",

        headers: {
          Authorization: "Bearer " + a.data.access_token,
          "Content-Type": "application/json"
        },
        url:
          "https://api.linkedin.com/v2/me?projection=(id,profilePicture(displayImage~:playableStreams))"
      })
        .then(a => {
          const profilePicUrl = a.data.profilePicture[
            "displayImage~"
          ].elements.slice(-1)[0].identifiers[0].identifier;

          //Cloudinary Upload
          cloudinary.v2.uploader.upload(profilePicUrl, (error, result) => {
            if (!error) {
              res.send({ picurl: result.secure_url.split("upload/")[1] });
            }
          });
        })
        .catch(e => {
          console.log("getttt error");
          console.log(e);
          res.send(e.response.data);
        });
    })
    .catch(e => {
      console.log(e);
      res.send(e.response.data);
    });
};
