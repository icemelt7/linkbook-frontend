import { string_to_slug } from '../../src/lib/helpers';
const sanityClient = require('@sanity/client');
const client = sanityClient({
  projectId: '5gyrk3u0',
  dataset: 'production',
  token: 'skV7w37FtGM1KKHg9R7LNrnhDlsgnd9ckvIrfalYgp9LtyCKxmoB3IERsdJfsohyl8GO9asB1cDpFx2uvgolTozBfkcnMA1RGvNliDOXBVjPJ9lLsoxenMdyZdRzbdJ3j9YI5iQYW9lPb3SYZPdmxVlUPWHTHPhdHsstGj48PEEzQEbuZ5iR',
  useCdn: false // `false` if you want to ensure fresh data
});

export default async (req, res) => {
  try {
    const sanityData = JSON.parse(req.body);
    sanityData._id = string_to_slug(sanityData.name);
    sanityData._type = "company";
    const company = await client.createOrReplace(sanityData);
    res.send(company);
  } catch (e) {
    console.log(JSON.stringify(e))
    res.status(500);
    res.send(e);
  }
};


