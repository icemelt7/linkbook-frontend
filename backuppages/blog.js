import React from "react";
require('isomorphic-fetch');
import Head from 'next/head';
import NavTail from "../src/components/NavTail";
// import { db } from "../src/lib/firebase";
import NewFooter from '../src/components/NewFooter'
import { POST_API } from "../src/lib/helpers";
// import spinner from '../src/spinner.gif';

// const firebase = await import('firebase/app')
// await import('firebase/database')


const BlogCards = ({ blogData }) => {
  return (
    <div className="mb-16 sm:flex" >
      <div className="shadow-lg sm:flex-shrink-0">
        <img className="h-24 sm:max200 sm:h-full w-full object-cover rounded-t-lg
                sm:rounded-lg no-max-w" src={blogData.jetpack_featured_media_url} alt="pic" />
      </div>
      <div className="sm:h-48 w-full sm:mt-1 px-10 pt-6 pb-8 sm:p-10 bg-white sm:rounded-tr-lg sm:rounded-r-lg sm:rounded-l-none rounded-b-lg shadow-lg overflow-y-hidden">
        <a href="#" className="block mb-4 sm:mb-1 sm:truncate literata text-xl text-black font-bold hover:underline">{blogData.title.rendered}</a>
        <p className="block sm:mb-3 text-xs leading-tight text-gray-600">20 July 2019</p>
        <div className="literata hidden sm:block sm:threeLines truncate md:whitespace-normal"
          dangerouslySetInnerHTML={{ __html: blogData.excerpt.rendered }}
        />
      </div>
    </div>
  )
}

const isAvail = (str) => {
  if (str === "") {
    str = "Information not Available"
  }
  return str
}
const Blog = ({ posts }) => {


  /* let data = [];

  for (let i = 0; i < 10; i++) {
      data.push({
          title: `Random post #${Math.floor(Math.random() * 10) + 1}`,
          subHeading: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.  consectetur adipiscing elit  consectetur adipiscing elit  consectetur adipiscing elit  consectetur adipiscing elit  consectetur adipiscing elit  consectetur adipiscing elit  consectetur adipiscing ",
          date: `${Math.floor(Math.random() * 10) + 1} July 2019`,
          img: `https://picsum.photos/id/${i+1}/200/?blur=2`
      })
  } */

  return (
    <div>
      <Head>
        <link href="https://fonts.googleapis.com/css?family=Literata&display=swap" rel="stylesheet" />
      </Head>
      <NavTail />
      {/* Container */}
      <div className="lg:w-1/2 pt-12 mx-8 lg:mx-auto">
        <p className="text-2xl font-bold">Articles and blogs by Automark</p>
        <p className="text-sm text-gray-600 mb-8">Collection of our thoughts, lessons, production updates and what we think of the future</p>
        <div className="">
          {/* <div className="bg-white p-5 rounded shadow-md"> */}
          {posts.map((blogData, i) => {
            return <BlogCards key={i} blogData={blogData} />
          })
          }
          {/* </div> */}
        </div>
      </div>
      <div className="flex justify-center mx-40 mb-16">
        <a href="/profilelist" className="bg-red-500 text-center hover:bg-red-700 text-white font-medium py-2 px-16 rounded">
          Go Back
        </a>
      </div>
      <div>
        <NewFooter />
      </div>
    </div>
  );
};

Blog.getInitialProps = async () => {
  const rawPosts = await fetch(`${POST_API()}/v2/posts/`);
  const posts = await rawPosts.json();
  return { posts };
}

export default Blog;
