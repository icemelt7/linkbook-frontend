import { firebase } from '../../src/lib/firebase';
import { useCollectionDataOnce } from 'react-firebase-hooks/firestore';
import { useAuthState } from 'react-firebase-hooks/auth';

const Index = () => {
  const [user, initialising, error] = useAuthState(firebase.auth());

  if (initialising) {
    return (
      <div>
        <p>Initialising User...</p>
      </div>
    );
  }
  if (error) {
    return (
      <div>
        <p>Error: {error}</p>
      </div>
    )
  }
  if (user) {
    return (
      <div>
        <UserData />
      </div>
    );
  }
  return <div>Please Authenticate</div>
};

export default Index;

const UserData = () => {
  const [values, loading, collectionError] = useCollectionDataOnce(firebase.firestore().collection('old_user'),
    {
      idField: "uid"
    });
  if (loading) {
    return <div>Loading Data</div>
  }
  return values && (
    <table>
      <tr>Name</tr>
      {values.map((doc, index) => (
        <tr className={`${index % 2 == 0 ? 'bg-gray-300' : 'bg-gray-100'}`}><td className="p-4">{doc.name}</td></tr>
      ))}
    </table>
  )
}