import React, { useState, useEffect } from "react";
import { useForm } from "../src/hooks/useForm";
import { updateUser } from "../src/lib/firebase";
import { useFirebaseUser } from "../src/lib/hooks";
import useLinkbookReducer from "../src/lib/linkbookReducer";
import NavTail from "../src/components/NavTail";
import LinkedInPage from "../src/components/Linkedin/LinkedInPage";
import AddressForm from "../src/components/AddressForm";
import { ToastContainer, toast } from 'react-toastify';
// import './dashboard_style.css';

// Create a context
export const UserContext = React.createContext();

toast.configure();

export default function Dashboard() {
  useEffect(() => {
    var node = document.createElement("script");
    node.src = "https://widget.cloudinary.com/v2.0/global/all.js";
    node.defer = true;
    document.head.appendChild(node);
  }, []);
  const [values, dispatch] = useLinkbookReducer();
  const notify = () => toast.success("Profile Updated");
  const [formStatus, setFormStatus] = useState("initial");
  useFirebaseUser(dispatch);
  const { current } = values;
  const { onChange } = useForm(dispatch);


  const saveInfo = async () => {
    setFormStatus("saving");
    await updateUser({ values });
    notify();
    setTimeout(() => {
      setFormStatus("saved");
      setTimeout(() => {
        setFormStatus("initial");
      }, 2000);
    }, 1000);
  };

  return (
    <div>
      <UserContext.Provider value={values.uid}>
        <NavTail />
        {current === "loaded" ? (
          <div>
            <div className="justify-center text-center bg-white  border-b border-gray-800">
              <p className="text-3xl font-medium py-2 ">Profile details</p>
              <p className="text-xs pb-3" >You can save and come back later</p>
              <LinkedInPage onChange={onChange} dispatch={dispatch} />
              <p className="text-base pb-3">Or</p>
            </div>
            <div className="px-6" style={{ backgroundColor: '#e6e6e66b' }}>
              <AddressForm
                values={values}
                formStatus={formStatus}
                onChange={onChange}
                saveInfo={saveInfo}
              />
            </div>
          </div>
        ) : (
            <div className="flex items-center text-center justify-center h-screen" style={{ backgroundColor: '#e6e6e66b' }}>
              <p className="text-lg text-black">Loading</p>
            </div>
          )}
      </UserContext.Provider>
    </div>
  );
}
