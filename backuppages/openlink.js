import React, { useEffect, useState } from "react";

export default function openlink() {
  const [mailink, setMaillink] = useState(null);
  useEffect(() => {
    if (typeof window !== "undefined") {
      setMaillink(`https://${window.location.search.split("=")[1]}`);
    }
  }, []);

  return (
    <div
      className="w-screen h-screen justify-center flex items-center overflow-hidden bg-gray-200"

    >
      <div className="max-w-sm sm:mx-0 mx-6 px-6 py-4 pt-10 bg-white shadow-lg rounded-lg relative">
        <p className=" text-2xl tracking-tight font-medium text-center">
          Confirm this Sign In
        </p>
        <img
          className="block mx-auto sm:h-40 h-24 pr-8"
          src="../static/images/email_sent1.png"
        />
        <div className="text-center">
          <p className="text-xs tracking-normal text-gray-700 font-medium">
            Check your email for the sign-in confirmation link sent to your
            email address
          </p>
          <a href={mailink} target="_blank">
            <div className="bg-red-500 hover:bg-red-600 text-white font-bold py-2 px-4 rounded text-center w-full mt-4">
              Open Email
          </div>
          </a>
          <a
            href="/emailsignin"
            className="block mt-5 mb-2 text-xs leading-loose font-medium text-red-600 hover:text-red-700 hover:underline"
          >
            Not in inbox or spam folder? Resend Back
          </a>
        </div>
      </div>
    </div>
  );
}
