import fetcher from "../src/lib/fetcher";
import FooterBlog from "../src/components/FooterBlog";
import CompanyBlogs from "../src/components/CompanyBlogs";
import NavBar from "../src/components/Blog/NavBar";
import NavBarAuth from "../src/components/Header/NavBar";
import React, { useState } from 'react'
import { POST_API } from "../src/lib/helpers";


const Companyprofile = ({ e, companies }) => {
  const [show, setShow] = useState(false)
  if (e) {
    return <span className="text-red-50 bg-red-500">{JSON.stringify(e)}</span>
  }
  return (
    <>
      {show && <NavBarAuth />}
      <div className=" max-w-6xl mx-auto">

        <NavBar />
        {/* <PostList posts={posts} /> */}
        <div className="md:max-w-none max-w-xs block mt-10 md:px-16 md:mx-0 mx-auto">
          <Title title="Most Recent" />
        </div>
        <div className="md:flex md:px-16 md:justify-between md:flex-wrap">
          <PostList posts={companies} />
          <div className="w-1/5 h-64 bg-red-500 mt-16 flex flex-grow mx-auto"></div>
        </div>
      </div>
      <FooterBlog />
    </>
  );
};
const Title = ({ title }) => {
  return (
    <>
      <p className="text-3xl font-bold leading-6 text-cool-gray-900  mb-3">
        {title}
      </p>
      <div className="border-b border-orange-500 w-8 h-1 mb-12"></div>
    </>
  );
};

const PostList = ({ posts }) => (
  <div className="md:pr-4 md:px-0 px-12 mt-16 md:max-w-screen-md">
    {posts.map(
      ({
        slug,
        excerpt: { rendered: summary },
        date,
        title: { rendered: postTitle },
        jetpack_featured_media_url: postImgUrl
      }) => {
        return (
          <CompanyBlogs
            slug={slug}
            postImgUrl={postImgUrl}
            postTitle={postTitle}
            date={date}
          />
        );
      }
    )}
  </div>
);


export async function getStaticProps() {
  try {
    let companies = await fetcher(`${POST_API()}/v2/company_profile`);
    companies = companies.map(async (company) => {
      const href = await fetcher(company._links['wp:attachment'][0].href);
      const image = href?.[0]?.guid?.rendered;
      return {
        ...company,
        ...image && { jetpack_featured_media_url: image }
      };
    })
    companies = await Promise.all(companies);
    return {
      props: {
        companies
      }
    };
  } catch (e) {
    return {
      props: {
        e
      }
    }
  }
}
export default Companyprofile;
