import React, { useEffect, useState } from 'react';
import clsx from 'clsx';

















import { mainListItems, secondaryListItems } from '../src/components/listItems';
// import Chart from '../src/components/Chart';
// import Deposits from '../src/components/Deposits';
// import Orders from '../src/components/Orders';
import CompanyForm from '../src/components/CompanyForm';
import { useForm } from '../src/hooks/useForm';
// import { db, updateUser } from '../src/lib/firebase';
import { useFirebaseUser } from '../src/lib/hooks';

import { Logger } from '../src/lib/Logger';
import { WP_CONTENT_API } from '../src/lib/helpers';

function MadeWithLove() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Built with love by the '}
      <Link color="inherit" href="https://automark.pk/">
        Automark
      </Link>
      {' team.'}
    </Typography>
  );
}

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  progress: {
    margin: '20px auto'
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
    backgroundColor: '#e71e24'
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
    backgroundColor: '#f0f8ff'
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
}));

export default function CompanyFormPage() {
  const classes = useStyles();
  useFirebaseUser();
  const [formStatus, setFormStatus] = useState('initial');
  const [values, onChange, setValues] = useForm({
    address: "",
    city: "",
    logo: "",
    name: "",
    phone_number: "",
    tags: [],
  });
  useEffect(() => {
    if (current === "loaded") {
      //Load The Company
      user.company.get().then(doc => {
        Logger.log(doc.data())
        setValues({ ...doc.data() });
      })
    }
    if (current === "notexist") {
      //  window.location.href = "/";
    }
  }, [current])
  const [open, setOpen] = useState(true);
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
  const saveInfo = async () => {
    setFormStatus('saving');
    await updateUser({ values, uid: user.uid });
    setTimeout(() => {
      setFormStatus('saved');
      setTimeout(() => {
        setFormStatus('initial');
      }, 2000);
    }, 1000);
  }
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="Open drawer"
            onClick={handleDrawerOpen}
            className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
          >
            <MenuIcon />
          </IconButton>
          <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
            Manage profile
          </Typography>
          <IconButton color="inherit">
            <Badge badgeContent={4} color="secondary">
              <NotificationsIcon />
            </Badge>
          </IconButton>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
      >

        <div className={classes.toolbarIcon}>
          <img src={`${WP_CONTENT_API()}/uploads/2018/02/Automark_New_logo.jpg`} alt="automark logo" width="140" />
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <Divider />
        <List>{mainListItems}</List>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>
          <Grid container spacing={3}>
            {/* Recent Orders */}
            <Grid item xs={12}>
              {current === "loaded" ? <CompanyForm values={values} formStatus={formStatus} onChange={onChange} saveInfo={saveInfo} /> : <CircularProgress className={classes.progress} />}
            </Grid>
          </Grid>
        </Container>
        <MadeWithLove />
      </main>
    </div>
  );
}