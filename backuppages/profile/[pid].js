import React from "react";
import NProgress from "nprogress";
import NavTail from "../../src/components/NavTail";
import NewFooter from "../../src/components/NewFooter";
import { S } from "../../src/lib/firebase";
import useLinkbookReducer from "../../src/lib/linkbookReducer";
import { Logger } from "../../src/lib/Logger";
import NameAndImage from "../../src/components/Profile/NameAndImage";
import Aboutme from "../../src/components/Profile/Aboutme";
import BasicInformation from "../../src/components/Profile/BasicInformation";
import { useFirebaseUser } from "../../src/lib/hooks";
import BackButton from "../../src/components/Profile/BackButton";
import BasicInformationLoggedOut from "../../src/components/Profile/BasicInformationLoggedOut";
// import spinner from '../src/spinner.gif';

// const firebase = await import('firebase/app')
// await import('firebase/database')

const Profile = ({ user }) => {
  const [values, dispatch] = useLinkbookReducer();
  useFirebaseUser(dispatch)
  const { current } = values;
  console.log(user)
  let Information = null
  switch (current) {
    case 'init': {
      Information = <div className="p-20 flex justify-center align-middle">
        <p className="text-4xl">Loading</p>
      </div>;
      break;
    }
    case 'notexist': {
      Information = <BasicInformationLoggedOut user={user} />;
      break;
    }
    default: Information = <BasicInformation user={user} />;
  }
  return (
    <div className="bg-gray-200">
      <NavTail />
      <div className="max-w-5xl mx-auto py-8">
      <NameAndImage user={user} />
      {/* Container */}
        <div className="mxs-16 mt-5 mb-5">
          <div className="md:flex flex-wrap -mx-2">
            <Aboutme user={user} />
            <div className="md:w-2/5 px-2">
              {Information}
            </div>
          </div>
        </div>
      <BackButton />
      </div>
      <NewFooter />
    </div>
  );
};

Profile.getInitialProps = async ({ query, res, req }) => {
  const { pid } = query;
  Logger.log(pid);
  if (res) {
    res.setHeader("Cache-Control", `s-maxage=10, stale-while-revalidate`);
  }
  //Logger.log(context)
  if (pid) {
    const query = `*[_type == "profile" && slug == "${pid}"]{
      ..., current_job_company_beta->
    }`;
    const profiles = await S.fetch(query);
    if (!req) {
      NProgress.done();
    }

    // Logger.log("Getting info");

    return { user: profiles[0] };
  }

  return {};
};

export default Profile;
