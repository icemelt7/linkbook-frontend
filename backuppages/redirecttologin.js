import React from 'react';

import { useFirebaseUser } from '../src/lib/hooks';
import useLinkbookReducer from '../src/lib/linkbookReducer';

export default function Index() {
  const [values, dispatch] = useLinkbookReducer();
  useFirebaseUser(dispatch);
  const { current } = values;
  if (current === 'notexist') {
    window.location.href = '/emailsignin';
  }

  if (current === 'loaded') {
    window.location.href = '/dashboard';
  }
  return (
    <div>
      <div className="my-4">
        {(current === 'initial' || current === 'notexist') && <p> Please wait while we log you in</p>}
        {current === 'loaded' && <div className="my-4">
          <p>
            Welcome {values.email}, redirecting you to your dashboard
          </p>
        </div>}
      </div>
    </div>
  );
}
