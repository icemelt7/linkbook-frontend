import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { verifyEmail, createUser } from '../src/lib/firebase';
import { Logger } from '../src/lib/Logger';

const propTypes = {};

const defaultProps = {};

export default function signincomplete(props) {
  const [user, setUser] = useState({});
  useEffect(() => {
    verifyEmail().then(async (result) => {
      Logger.log(result)
      setUser(result);

      // If he's a new user, create a profile, otherwise update the existing one
      try {
        await createUser(result);
        window.location.href = "/dashboard";
      } catch (e) {
        Logger.log(result)
        Logger.log(e);
      }
    })
  }, [])
  return (
    <div className="w-screen h-screen justify-center overflow-hidden flex items-center">
      <div className="bg-blue-100 border-t border-b border-blue-500 text-blue-700 px-4 py-4 w-screen text-center" role="alert">
        <p className="font-bold text-lg py-2">Thankyou for your patience</p>
        <p className="text-lg py-2">Please wait while we verify you.</p>
      </div>
    </div>
  );
}

signincomplete.propTypes = propTypes;
signincomplete.defaultProps = defaultProps;