import React, { useEffect, useState } from "react";
import useLinkbookReducer from "../src/lib/linkbookReducer";
import { useFirebaseUser } from "../src/lib/hooks";
import { Logger } from "../src/lib/Logger";
import { S } from "../src/lib/firebase";
import FeaturedProfiles from "../src/components/Index/FeaturedProfiles";
function LandingPage() {
  const [values, dispatch] = useLinkbookReducer();
  const [profiles, setProfiles] = useState(null);
  const { current } = values;

  useFirebaseUser(dispatch);
  Logger.log(current)
  if (current === "loaded") {
    // window.location.href = "/dashboard";
  }

  useEffect(() => {
    const query = `*[_type == "profile"][0...5]`;
    S.fetch(query).then(profiles => {
      setProfiles(profiles);
    });
  }, [])

  return (
    <>
      <div className="lg:pt-24 pt-16 min-h-screen" style={{ background: "linear-gradient(90deg, #d53333 0%, #ff000061 100%)" }}>
        <div className="container px-3 mx-auto flex flex-wrap flex-col lg:flex-row items-center">
          {/* <!--Left Col--> */}
          <div className="flex flex-col w-full lg:w-3/5 justify-center items-start text-center lg:text-left text-white">
            <p className="uppercase tracking-loose w-full text-2xl">Welcome to Linkbook</p>
            <h1 className="my-4 text-5xl font-bold leading-tight">Get Noticed by Companies all over Pakistan</h1>
            <p className="leading-normal text-2xl mb-8">Quickly create your professional profile. And share it accross Facebook, LinkedIn and email to everyone</p>

            <a href="/emailsignin" className="mx-auto lg:mx-0 hover:underline bg-white text-gray-800 font-bold rounded-full my-6 py-4 px-8 shadow-lg">
              Start Making your profile now
            </a>
            {current === "init" && <a href="/emailsignin" className="text-white pl-0 lg:pl-6 mb-4 mx-auto lg:mx-0 hover:underline">Trying to log you in</a>}
            {current === "notexist" && <a href="/emailsignin" className="text-white pl-0 lg:pl-6 mb-4 mx-auto lg:mx-0 hover:underline">Already a member? Log In</a>}
            {current === "loaded" && <a href="/emailsignin" className="text-white pl-0 lg:pl-6 mb-4 mx-auto lg:mx-0 hover:underline">Redirecting to your dashboard, please wait</a>}

          </div>
          {/* <!--Right Col--> */}
          <div className="w-full lg:w-2/5 py-6 text-center">
            <img className="hidden lg:block lg:w-4/5 w-1/4 z-50" src="hero.png" />
          </div>
        </div>
      </div>
      <section className="bg-gray-200 border-b py-4">
        <div className="container max-w-5xl mx-auto m-8">
          {profiles && <FeaturedProfiles profiles={profiles} />}
        </div>
      </section>
    </>
  );
}

export default LandingPage;
